import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

home = str(Path.home())
plots_path = home + "/git/e2clab-examples/cctv/cluster-2020/plots"


def autolabel(rects, xpos='center'):
    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0, 'right': 1, 'left': -1}

    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(offset[xpos]*3, 3),  # use 3 points offset
                    textcoords="offset points",  # in both directions
                    ha=ha[xpos], va='bottom')


def from_csv_to_arr(csv_file_path):
    data = []
    for csv_file in csv_file_path:
        # print(csv_file)
        f = open(csv_file, 'r')
        n_messages = 0
        time_in_ms = 0
        l_idx = 0
        for l in f.readlines():
            if l_idx == 0:
                n_messages = int(l.split(':')[1])
            else:
                time_in_ms = int(l.split(':')[1])
            l_idx += 1
        msgs_s = (n_messages / (time_in_ms / 1000))
        data.append(msgs_s)
        # print(n_messages)
        # print(time_in_ms)
        # print(round(msgs_s))
    print(data)
    return data


def gen_path_to_files(ids, name, exp_id, cluster, site):
    paths = []
    for id in ids:
        paths.append(f"./{exp_id}/experiment-results/gateways/{cluster}-{id}.{site}.grid5000.fr/opt/metrics/throughput")
    return paths


def format_value(values):
    new_val = []
    for val in values:
        new_val.append(float("{:.2f}".format(val)))
    return new_val


hybrid_ids = [57, 66, 67, 69, 58, 59, 6, 60, 61, 62, 64, 65]
cloud_ids = [57, 66, 67, 69, 58, 59, 6, 60, 61, 62, 64, 65]

# CSV FILES
hybrid_s3_20c_csv_files = gen_path_to_files(hybrid_ids, 'hybrid_s3_20c', '20200523-195646', 'gros', 'nancy')
hybrid_s3_30c_csv_files = gen_path_to_files(hybrid_ids, 'hybrid_s3_30c', '20200523-200902', 'gros', 'nancy')
hybrid_s3_40c_csv_files = gen_path_to_files(hybrid_ids, 'hybrid_s3_40c', '20200523-202126', 'gros', 'nancy')

cloud_s3_20c_csv_files = gen_path_to_files(cloud_ids, 'cloud_s3_20c', '20200523-233352', 'gros', 'nancy')
cloud_s3_30c_csv_files = gen_path_to_files(cloud_ids, 'cloud_s3_30c', '20200523-234558', 'gros', 'nancy')
cloud_s3_40c_csv_files = gen_path_to_files(cloud_ids, 'cloud_s3_40c', '20200523-235832', 'gros', 'nancy')

# RAW DATA
hybrid_s3_20c = np.array(from_csv_to_arr(hybrid_s3_20c_csv_files))
hybrid_s3_30c = np.array(from_csv_to_arr(hybrid_s3_30c_csv_files))
hybrid_s3_40c = np.array(from_csv_to_arr(hybrid_s3_40c_csv_files))

cloud_s3_20c = np.array(from_csv_to_arr(cloud_s3_20c_csv_files))
cloud_s3_30c = np.array(from_csv_to_arr(cloud_s3_30c_csv_files))
cloud_s3_40c = np.array(from_csv_to_arr(cloud_s3_40c_csv_files))

# AVERAGE
hybrid_s3_20c_mean = np.mean(hybrid_s3_20c)
hybrid_s3_30c_mean = np.mean(hybrid_s3_30c)
hybrid_s3_40c_mean = np.mean(hybrid_s3_40c)

cloud_s3_20c_mean = np.mean(cloud_s3_20c)
cloud_s3_30c_mean = np.mean(cloud_s3_30c)
cloud_s3_40c_mean = np.mean(cloud_s3_40c)

# STANDARD DEVIATION
hybrid_s3_20c_std = np.std(hybrid_s3_20c)
hybrid_s3_30c_std = np.std(hybrid_s3_30c)
hybrid_s3_40c_std = np.std(hybrid_s3_40c)

cloud_s3_20c_std = np.std(cloud_s3_20c)
cloud_s3_30c_std = np.std(cloud_s3_30c)
cloud_s3_40c_std = np.std(cloud_s3_40c)


scenarios = ['20', '30', '40']

CTEs_h = [hybrid_s3_20c_mean, hybrid_s3_30c_mean, hybrid_s3_40c_mean]
CTEs_c = [cloud_s3_20c_mean, cloud_s3_30c_mean, cloud_s3_40c_mean]

error_h = [hybrid_s3_20c_std, hybrid_s3_30c_std, hybrid_s3_40c_std]
error_c = [cloud_s3_20c_std, cloud_s3_30c_std, cloud_s3_40c_std]

_color_hybrid = 'slategray'
_color_cloud = 'lightsteelblue'
ind = np.arange(len(CTEs_h))
width = 0.35
# Build the plot
fig, ax = plt.subplots()
rects1 = ax.bar(ind - width/2, format_value(CTEs_h), width, yerr=error_h, color=_color_hybrid, label='Hybrid')
rects2 = ax.bar(ind + width/2, format_value(CTEs_c), width, yerr=error_c, color=_color_cloud, label='Cloud')


_fontsize = 12
plt.rcParams.update({'font.size': _fontsize})
ax.set_ylabel('Messages/second processed', fontsize=_fontsize+1)
ax.set_xlabel('Number of Cameras', fontsize=_fontsize+1)
ax.set_title('Gateway Performance 1Gb (E-F) / 1Gb (F-C)', fontsize=_fontsize+1)
plt.xticks(fontsize=_fontsize+1)
plt.yticks(fontsize=_fontsize+1)

ax.set_xticks(ind)
ax.set_xticklabels(scenarios)
ax.legend( loc="upper left",  bbox_transform=fig.transFigure)

ax.yaxis.grid(True)

# Save the figure and show
autolabel(rects1, "left")
autolabel(rects2, "right")
plt.tight_layout()
plt.savefig(f"{plots_path}/gw-throughput-v2-1gbit-1gbit.png")
plt.show()





