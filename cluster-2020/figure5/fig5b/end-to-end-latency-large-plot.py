import matplotlib.pyplot as plt
from pathlib import Path

home = str(Path.home())
plots_path = home + "/git/e2clab-examples/cctv/cluster-2020/plots"
_fontsize = 12


def check_dict(id, _dict):
    sum_l = 0
    sum_m = 0
    for lts in _dict["latency"]:
        sum_l += lts
    for msg in _dict["messages"]:
        sum_m += msg
    print(f"[check_dict]{id} - len[latency]={len(_dict['latency'])}, max[latency]={max(_dict['latency'])}, sum_m={sum_m}")


def from_csv_to_dict(csv_file_path, scn_id):
    f = open(csv_file_path, "r")
    file_data = f.read()
    csv_values = iter(file_data.split(' '))
    next(csv_values)
    latency_values = []
    message_values = []
    for _data in csv_values:
        d = _data.split(',')
        latency_values.append(int(d[0]))
        message_values.append(int(d[1]))

    latency_dict = {"latency": latency_values, "messages": message_values}
    all_latency_arr = []
    for idx in range(len(message_values)):
        for num_of_msgs in range(message_values[idx]):
            all_latency_arr.append(latency_values[idx])

    print('all_latency_arr = ', all_latency_arr)
    print(f"VALIDATE: sum(message_values) = {sum(message_values)} == len(all_latency_arr) = {len(all_latency_arr)}, "
          f"MAX = {max(all_latency_arr)}, MIN = {min(all_latency_arr)}, MEAN = {sum(all_latency_arr)/len(all_latency_arr)}")
    return all_latency_arr


def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)


hybrid_s2_e2e_s = "./22mbit-1gbit/20200527-083852/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
cloud_s2_e2e_s = "./22mbit-1gbit/20200527-100339/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_s2_e2e_s_dict = from_csv_to_dict(hybrid_s2_e2e_s, 'hybrid_m2')
cloud_s2_e2e_s_dict = from_csv_to_dict(cloud_s2_e2e_s, 'cloud_m2')

hybrid_m2_e2e_m = "./1gbit-1gbit/20200527-105649/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
cloud_m2_e2e_m = "./1gbit-1gbit/20200527-102626/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_m2_e2e_m_dict = from_csv_to_dict(hybrid_m2_e2e_m, 'hybrid_m3p')
cloud_m2_e2e_m_dict = from_csv_to_dict(cloud_m2_e2e_m, 'cloud_m3p')

hybrid_l2_e2e_l = "./1gbit-22mbit/20200527-111202/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
cloud_l2_e2e_l = "./1gbit-22mbit/20200527-103402/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_l2_e2e_l_dict = from_csv_to_dict(hybrid_l2_e2e_l, 'hybrid_m2p')
cloud_l2_e2e_l_dict = from_csv_to_dict(cloud_l2_e2e_l, 'cloud_m2p')


scenarios = [hybrid_s2_e2e_s_dict, cloud_s2_e2e_s_dict,
             hybrid_m2_e2e_m_dict, cloud_m2_e2e_m_dict,
             hybrid_l2_e2e_l_dict, cloud_l2_e2e_l_dict]

data1 = [scenarios[0], scenarios[1]]
data2 = [scenarios[2], scenarios[3]]
data3 = [scenarios[4], scenarios[5]]

fig, ax = plt.subplots()

new_lbl = ['22mbit - 1gbit', '1gbit - 1gbit', '1gbit - 22mbit']

medianprops = dict(linestyle='-', linewidth=1.5, color='black')
bplot1 = ax.boxplot(data1,
                    positions=[-0.4, 0.4], sym='',
                    widths=0.4,
                    vert=True,  # vertical box alignment
                    patch_artist=True,  # fill with color
                    labels=None,
                    medianprops=medianprops)  # will be used to label x-ticks

bplot2 = ax.boxplot(data2,
                    positions=[1.6, 2.4], sym='',
                    widths=0.4, vert=True, patch_artist=True, labels=None, medianprops=medianprops)

bplot3 = ax.boxplot(data3,
                    positions=[3.6, 4.4],
                    widths=0.4, vert=True, patch_artist=True, labels=None, medianprops=medianprops)


_color_a = 'slategray'
_color_b = 'lightsteelblue'
colors = [_color_a, _color_b]
for bplot in (bplot1, bplot2, bplot3):
    for patch, color in zip(bplot['boxes'], colors):
        patch.set_facecolor(color)


plt.rcParams.update({'font.size': _fontsize})
ax.set_ylabel('Latency (in ms)', fontsize=_fontsize+1)
ax.set_title('End-to-end Latency (Large Workload)', fontsize=_fontsize+1)
plt.xticks(fontsize=_fontsize+1)
plt.yticks(fontsize=_fontsize+1)
plt.xticks(range(0, len(new_lbl) * 2, 2), new_lbl)
ax.legend([bplot1["boxes"][0], bplot1["boxes"][1]], ['Hybrid', 'Cloud'], loc="upper left", bbox_transform=fig.transFigure)
fig.tight_layout()
plt.savefig(f"{plots_path}/e2e-l-figure-1-L-3-Networks.png")
plt.show()
