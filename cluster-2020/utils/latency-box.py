import csv
import pandas as pd
import matplotlib.pyplot as plt


plots_path = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/plots"
plot_file_name = "end-to-end-latency.png"
_title = "End-to-end Latency"
x_label = 'Latency (ms)'
y_label = 'messages (in %)'
plt.figure(figsize=(6, 3))
_fontsize = 10


def all_messages(values):
    new_values = []
    _latency = 1
    for qtd in values:
        for i in range(int(qtd)):
            new_values.append(_latency)
        _latency += 1
    print("new_values =", new_values)
    return new_values


def print_dict(id, _dict, h1, h2):
    print(f"{id} = {_dict}")
    print(f"{id} len[{h1}] = {len(_dict[h1])}")
    print(f"{id} len[{h2}] = {len(_dict[h2])}")


def check_dict(id, _dict):
    sum_l = 0
    sum_m = 0
    for lts in _dict["latency"]:
        sum_l += lts
    for msg in _dict["messages"]:
        sum_m += msg
    print(f"[check_dict]{id} - len[latency]={len(_dict['latency'])}, max[latency]={max(_dict['latency'])}, sum_m={sum_m}")


def from_csv_to_dict(csv_file_path, scn_id):
    f = open(csv_file_path, "r")
    file_data = f.read()
    csv_values = iter(file_data.split(' '))
    next(csv_values)
    next(csv_values)
    latency_values = []
    message_values = []
    # idx_values = 1
    for _data in csv_values:
        # if idx_values > 2:
        #     break
        d = _data.split(',')
        latency_values.append(int(d[0]))
        message_values.append(int(d[1]))
        # idx_values += 1

    latency_dict = {"latency": latency_values, "messages": message_values}
    print_dict('source', latency_dict, "latency", "messages")

    all_lat = max(latency_dict["latency"])
    all_latency_dict = {"id": scn_id, "latency": [], "messages": []}

    # print("all_lat = ", all_lat)
    idx_lat = 1
    for lat in range(all_lat):
        all_latency_dict["latency"].append(idx_lat)
        all_latency_dict["messages"].append(0)
        idx_lat += 1

    for lat in all_latency_dict["latency"]:
        if lat in latency_dict["latency"]:
            all_latency_dict["messages"][lat - 1] = latency_dict["messages"][latency_dict["latency"].index(lat)]

    print_dict('zeros', all_latency_dict, "latency", "messages")
    check_dict('source', latency_dict)
    check_dict('zeros', all_latency_dict)
    return all_latency_dict



# E2C FIXME percentage of messages with a latency of 1ms
# hybrid_1_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/experiment-results/sinks/in-sink/paravance-4.rennes.grid5000.fr/opt/metrics/in-sink/latency"
# hybrid_4_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/experiment-results/sinks/in-sink/paravance-56.rennes.grid5000.fr/opt/metrics/in-sink/latency"
# cloud_1_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_1/20200512-180112/experiment-results/sinks/in-sink/paravance-4.rennes.grid5000.fr/opt/metrics/in-sink/latency"
# cloud_4_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_4/20200513-051923/experiment-results/sinks/in-sink/paravance-71.rennes.grid5000.fr/opt/metrics/in-sink/latency"
# hybrid_1_dict_e2c = from_csv_to_dict(hybrid_1_dstat_e2c, 'hybrid_1')
# cloud_1_dict_e2c = from_csv_to_dict(cloud_1_dstat_e2c, 'cloud_1')
# hybrid_4_dict_e2c = from_csv_to_dict(hybrid_4_dstat_e2c, 'hybrid_4')
# cloud_4_dict_e2c = from_csv_to_dict(cloud_4_dstat_e2c, 'cloud_4')


# E2E
# hybrid_1_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/experiment-results/sinks/out-sink/paravance-4.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_2_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_2/20200513-124437/experiment-results/sinks/out-sink/paravance-54.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_3_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_3/20200514-003542/experiment-results/sinks/out-sink/paravance-35.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_4_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/experiment-results/sinks/out-sink/paravance-56.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_1_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_1/20200512-180112/experiment-results/sinks/out-sink/paravance-4.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_2_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_2/20200513-150450/experiment-results/sinks/out-sink/paravance-54.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_3_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_3/20200514-042418/experiment-results/sinks/out-sink/paravance-35.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_4_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_4/20200513-051923/experiment-results/sinks/out-sink/paravance-71.rennes.grid5000.fr/opt/metrics/out-sink/latency"
#
# hybrid_1_dict = from_csv_to_dict(hybrid_1_dstat_e2e, 'hybrid_1')
# cloud_1_dict = from_csv_to_dict(cloud_1_dstat_e2e, 'cloud_1')
#
# hybrid_2_dict = from_csv_to_dict(hybrid_2_dstat_e2e, 'hybrid_2')
# cloud_2_dict = from_csv_to_dict(cloud_2_dstat_e2e, 'cloud_2')
#
# hybrid_3_dict = from_csv_to_dict(hybrid_3_dstat_e2e, 'hybrid_3')
# cloud_3_dict = from_csv_to_dict(cloud_3_dstat_e2e, 'cloud_3')
#
# hybrid_4_dict = from_csv_to_dict(hybrid_4_dstat_e2e, 'hybrid_4')
# cloud_4_dict = from_csv_to_dict(cloud_4_dstat_e2e, 'cloud_4')
#
# # NEW FREQUENCY
# hybrid_4_dstat_e2e_f10 = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_4_f10/20200516-140942/experiment-results/sinks/out-sink/paravance-51.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_4_dstat_e2e_f10 = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_4_f10/20200516-154353/experiment-results/sinks/out-sink/paravance-51.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_4_dict = from_csv_to_dict(hybrid_4_dstat_e2e, 'hybrid_4')
# cloud_4_dict = from_csv_to_dict(cloud_4_dstat_e2e, 'cloud_4')
# hybrid_4_dict_f10 = from_csv_to_dict(hybrid_4_dstat_e2e_f10, 'hybrid_4_f')
# cloud_4_dict_f10 = from_csv_to_dict(cloud_4_dstat_e2e_f10, 'cloud_4_f')
#
# hybrid_1_dstat_e2e_f10 = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_1_f10/20200515-204622/experiment-results/sinks/out-sink/paravance-35.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_1_dstat_e2e_f10 = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_1_f10/20200515-215623/experiment-results/sinks/out-sink/paravance-35.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_1_dict = from_csv_to_dict(hybrid_1_dstat_e2e, 'hybrid_1')
# cloud_1_dict = from_csv_to_dict(cloud_1_dstat_e2e, 'cloud_1')
# hybrid_1_dict_f10 = from_csv_to_dict(hybrid_1_dstat_e2e_f10, 'hybrid_1_f')
# cloud_1_dict_f10 = from_csv_to_dict(cloud_1_dstat_e2e_f10, 'cloud_1_f')

def plot_data_box_plot(_df, file_path):
    _df.plot.box(rot=20)
    plt.title(_title)
    plt.ylabel(x_label, fontsize=_fontsize)
    # plt.ylim(0, 60)
    plt.savefig(file_path)


def plot_data_bar_plot(_df, file_path):
    _df.plot.bar()
    plt.title("Edge-to-Cloud Latency")
    plt.xlabel('latency of 1ms', fontsize=_fontsize)
    plt.ylabel(y_label, fontsize=_fontsize)
    plt.savefig(file_path)


def calc_relative_freq_of_msgs(_dict):
    key = "messages"
    _dataframe = pd.DataFrame(data=_dict)
    dataframe_rel = _dataframe[key] / _dataframe[key].sum()
    _dataframe[key] = round(dataframe_rel * 100)
    print(f">>> dataframe[{key}].sum() = {int(_dataframe[key].sum())}")
    return _dataframe, int(_dataframe[key].sum())


def add_scenario_to_dataframe(scenarios, df):
    min_len = 100
    _rel_freqs = []

    for scenario in scenarios:
        _rel_freq, _size = calc_relative_freq_of_msgs(scenario)
        _rel_freqs.append({'id': scenario['id'], 'rf': _rel_freq})
        min_len = _size if min_len > _size else min_len
    print("selected min_len = ", min_len)

    for _rel_freq in _rel_freqs:
        # print(_rel_freq)  # print dataframe
        all_msgs = all_messages(_rel_freq['rf']['messages'])
        print(f">>> len of all_msgs = {len(all_msgs)}")
        df.insert(0, _rel_freq['id'], all_msgs[:min_len], True)


# ########################################## NEW
hybrid_l1_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l1/20200523-142759/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_l2_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l2/20200523-155454/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
cloud_l1_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l1/20200523-170223/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
cloud_l2_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l2/20200523-171549/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_l1_e2e_l_dict = from_csv_to_dict(hybrid_l1_e2e_l, 'hybrid_l1')
hybrid_l2_e2e_l_dict = from_csv_to_dict(hybrid_l2_e2e_l, 'hybrid_l2')
cloud_l1_e2e_l_dict = from_csv_to_dict(cloud_l1_e2e_l, 'cloud_l1')
cloud_l2_e2e_l_dict = from_csv_to_dict(cloud_l2_e2e_l, 'cloud_l2')


hybrid_m1_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_m1/20200524-013620/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_m2_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_m2/20200524-015302/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
cloud_m1_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_m1/20200524-025852/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
cloud_m2_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_m2/20200524-031519/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_m1_e2e_l_dict = from_csv_to_dict(hybrid_m1_e2e_l, 'hybrid_m1')
hybrid_m2_e2e_l_dict = from_csv_to_dict(hybrid_m2_e2e_l, 'hybrid_m2')
cloud_m1_e2e_l_dict = from_csv_to_dict(cloud_m1_e2e_l, 'cloud_m1')
cloud_m2_e2e_l_dict = from_csv_to_dict(cloud_m2_e2e_l, 'cloud_m2')


hybrid_s1_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_s1/20200524-112210/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_s2_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_s2/20200524-113816/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
cloud_s1_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_s1/20200524-124246/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
cloud_s2_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_s2/20200524-125329/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_s1_e2e_l_dict = from_csv_to_dict(hybrid_s1_e2e_l, 'hybrid_s1')
hybrid_s2_e2e_l_dict = from_csv_to_dict(hybrid_s2_e2e_l, 'hybrid_s2')
cloud_s1_e2e_l_dict = from_csv_to_dict(cloud_s1_e2e_l, 'cloud_s1')
cloud_s2_e2e_l_dict = from_csv_to_dict(cloud_s2_e2e_l, 'cloud_s2')


# E2E - ALL SCENARIOS
scenarios = [hybrid_l2_e2e_l_dict, cloud_l2_e2e_l_dict, hybrid_l1_e2e_l_dict, cloud_l1_e2e_l_dict,
             hybrid_m2_e2e_l_dict, cloud_m2_e2e_l_dict, hybrid_m1_e2e_l_dict, cloud_m1_e2e_l_dict,
             hybrid_s2_e2e_l_dict, cloud_s2_e2e_l_dict, hybrid_s1_e2e_l_dict, cloud_s1_e2e_l_dict]
df = pd.DataFrame(data={})
add_scenario_to_dataframe(scenarios, df)
print(df)
plot_data_box_plot(df, f"{plots_path}/tests-new/sse2e-sml_{plot_file_name}")


### ------------- NEW FREQUENCIES
# scenarios = [hybrid_4_dict, cloud_4_dict, hybrid_4_dict_f10, cloud_4_dict_f10, hybrid_1_dict, cloud_1_dict, hybrid_1_dict_f10, cloud_1_dict_f10]
# df = pd.DataFrame(data={})
# add_scenario_to_dataframe(scenarios, df)
# print(df)
# plot_data_box_plot(df, f"{plots_path}/tests/e2e-freq-10-1-4_{plot_file_name}")


# E2C
# scenarios = [hybrid_1_dict_e2c, cloud_1_dict_e2c, hybrid_4_dict_e2c, cloud_4_dict_e2c]
#
# data = {}
# for scenario in scenarios:
#     _rel_freq, _size = calc_relative_freq_of_msgs(scenario)
#     data.update({scenario['id']: _rel_freq['messages'][0]})
#     print(f"sum[{_size}] / freq = \n {_rel_freq['messages'][0]}")
#
# df = pd.DataFrame(data=data, index=[1])  # index=[0, 20, 40, 60, 80, 100]
# plot_data_bar_plot(df, f"{plots_path}/tests/e2c-rel4-freq_{plot_file_name}")
# df.plot.bar()


# df = pd.DataFrame([[10, 20, 30, 40], [7, 14, 21, 28], [15, 15, 8, 12],
#                    [15, 14, 1, 8], [7, 1, 1, 8], [5, 4, 9, 2]],
#                   columns=['Apple', 'Orange', 'Banana', 'Pear'],
#                   index=['Basket1', 'Basket2', 'Basket3', 'Basket4',
#                          'Basket5', 'Basket6'])
#
# box = plt.boxplot(df, patch_artist=True)
#
# colors = ['blue', 'green', 'purple', 'tan', 'pink', 'red']
#
# for patch, color in zip(box['boxes'], colors):
#     patch.set_facecolor(color)
#
# ----------------- plt.show()


# ########################################################### NEW
# hybrid_l1p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l1p/20200521-211227/experiment-results/sinks/out-sink/gros-51.nancy.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_l2p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l2p/20200521-213246/experiment-results/sinks/out-sink/gros-51.nancy.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_l3p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l3p/20200521-215255/experiment-results/sinks/out-sink/gros-51.nancy.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_l4p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l4p/20200521-221318/experiment-results/sinks/out-sink/gros-51.nancy.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_l1p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l1p/20200521-014638/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_l2p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l2p/20200521-020835/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_l3p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l3p/20200521-120135/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"  #"/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l3p/20200521-022308/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_l4p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l4p/20200521-152405/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_l1p_e2e_l_dict = from_csv_to_dict(hybrid_l1p_e2e_l, 'hybrid_l1p')
# hybrid_l2p_e2e_l_dict = from_csv_to_dict(hybrid_l2p_e2e_l, 'hybrid_l2p')
# hybrid_l3p_e2e_l_dict = from_csv_to_dict(hybrid_l3p_e2e_l, 'hybrid_l3p')
# hybrid_l4p_e2e_l_dict = from_csv_to_dict(hybrid_l4p_e2e_l, 'hybrid_l4p')
# # hybrid_l4p_3g_e2e_l_dict = from_csv_to_dict(hybrid_l4p_3g_e2e_l, 'hybrid_l4p_3g')  # FIXME run them on nancy grisou
# cloud_l1p_e2e_l_dict = from_csv_to_dict(cloud_l1p_e2e_l, 'cloud_l1p')
# cloud_l2p_e2e_l_dict = from_csv_to_dict(cloud_l2p_e2e_l, 'cloud_l2p')
# cloud_l3p_e2e_l_dict = from_csv_to_dict(cloud_l3p_e2e_l, 'cloud_l3p')
# cloud_l4p_e2e_l_dict = from_csv_to_dict(cloud_l4p_e2e_l, 'cloud_l4p')
# cloud_l4p_3g_e2e_l_dict = from_csv_to_dict(cloud_l4p_3g_e2e_l, 'cloud_l4p_3g')  # FIXME run them on nancy grisou

# hybrid_s1p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_s1p/20200521-183110/experiment-results/sinks/out-sink/parasilo-15.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_s2p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_s2p/20200521-184808/experiment-results/sinks/out-sink/parasilo-15.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_s3p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_s3p/20200521-190502/experiment-results/sinks/out-sink/parasilo-15.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_s1p_e2e_l_dict = from_csv_to_dict(hybrid_s1p_e2e_l, 'hybrid_s1p')
# hybrid_s2p_e2e_l_dict = from_csv_to_dict(hybrid_s2p_e2e_l, 'hybrid_s2p')
# hybrid_s3p_e2e_l_dict = from_csv_to_dict(hybrid_s3p_e2e_l, 'hybrid_s3p')
#
# cloud_s1p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_s1p/20200521-005204/experiment-results/sinks/out-sink/parasilo-28.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_s2p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_s2p/20200521-010850/experiment-results/sinks/out-sink/parasilo-28.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_s3p_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_s3p/20200521-012555/experiment-results/sinks/out-sink/parasilo-28.rennes.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_s1p_e2e_l_dict = from_csv_to_dict(cloud_s1p_e2e_l, 'cloud_s1p')
# cloud_s2p_e2e_l_dict = from_csv_to_dict(cloud_s2p_e2e_l, 'cloud_s2p')
# cloud_s3p_e2e_l_dict = from_csv_to_dict(cloud_s3p_e2e_l, 'cloud_s3p')
#
# scenarios = [#hybrid_l4p_e2e_l_dict, cloud_l4p_e2e_l_dict, hybrid_l3p_e2e_l_dict, cloud_l3p_e2e_l_dict, hybrid_l2p_e2e_l_dict, cloud_l2p_e2e_l_dict, hybrid_l1p_e2e_l_dict, cloud_l1p_e2e_l_dict] #,
#              # hybrid_m2p_e2e_l_dict, cloud_m2p_e2e_l_dict, hybrid_m1p_e2e_l_dict, cloud_m1p_e2e_l_dict,
#              hybrid_s3p_e2e_l_dict, cloud_s3p_e2e_l_dict, hybrid_s2p_e2e_l_dict, cloud_s2p_e2e_l_dict, hybrid_s1p_e2e_l_dict, cloud_s1p_e2e_l_dict]
# df = pd.DataFrame(data={})
# add_scenario_to_dataframe(scenarios, df)
# print(df)
# plot_data_box_plot(df, f"{plots_path}/tests-new/e2e-small-p4_{plot_file_name}")
