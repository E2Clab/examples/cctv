import csv
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.patches as mpatches

plots_path = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/plots"
# TO_MB = 1000000
# TIMEOUT = 220
# all_prod = []
# all_recv = []
# all_send = []
site = 'nancy'
cluster = 'gros'
NUM_PRODUCERS = 40


def autolabel(rects, xpos='center'):
    """
    Attach a text label above each bar in *rects*, displaying its height.

    *xpos* indicates which side to place the text w.r.t. the center of
    the bar. It can be one of the following {'center', 'right', 'left'}.
    """

    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0, 'right': 1, 'left': -1}

    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(offset[xpos]*3, 3),  # use 3 points offset
                    textcoords="offset points",  # in both directions
                    ha=ha[xpos], va='bottom')


def sink_in_sum_msgs(csv_file_path):
    f = open(csv_file_path, "r")
    file_data = f.read()
    csv_values = iter(file_data.split(' '))
    # next(csv_values) # FIXME skip latency label
    next(csv_values)
    latency_values = []
    message_values = []
    # idx_values = 1
    for _data in csv_values:
        # if idx_values > 2:
        #     break
        d = _data.split(',')
        latency_values.append(int(d[0]))
        message_values.append(int(d[1]))
        # idx_values += 1

    total_msgs = sum(message_values)
    print("SINK_IN total msngs = ", total_msgs)
    return total_msgs


def from_csv_to_arr(csv_file_path):
    data = []
    for csv_file in csv_file_path:
        # print(csv_file)
        f = open(csv_file, 'r')
        n_messages = 0
        time_in_ms = 0
        l_idx = 0
        for l in f.readlines():
            if l_idx == 0:
                n_messages = int(l.split(':')[1])
            elif l_idx == 1:
                time_in_ms = int(l.split(':')[1])
            l_idx += 1
        msgs_s = (n_messages / (time_in_ms / 1000))
        data.append(msgs_s)
        # if '_l1' in csv_file:
        #     if 'producers' in csv_file:
        #         print(f"th[{msgs_s}] = {csv_file}")
        # print(n_messages)
        # print(time_in_ms)
        # print(round(msgs_s))
    # print(data)
    remove_outliers(data)
    return data


def remove_outliers(_data):
    _mean = np.array(_data).mean()
    for _d in _data:
        if _d > 4*_mean:
            print(f"removing outlier = {_d}, mean[{_mean}]")
            _data.remove(_d)


def gen_path_to_files_gw(ids, name, exp_id, cluster, site):
    _prefix = '/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios'
    paths = []
    device = 'gateways'
    # device = 'sinks/out-sink'
    device_file = 'opt/metrics/throughput'
    # device_file = 'opt/metrics/out-sink/latency'
    for id in ids:
        paths.append(f"{_prefix}/{name}/{exp_id}/experiment-results/{device}/{cluster}-{id}.{site}.grid5000.fr/{device_file}")
    return paths

def gen_path_to_files_pd(ids, name, exp_id, cluster, site):
    _prefix = '/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios'
    paths = []
    for _id in ids:
        id_cam = 1
        for cam in range(NUM_PRODUCERS):
            paths.append(f"{_prefix}/{name}/{exp_id}/experiment-results/producers/{cluster}-{_id}.{site}.grid5000.fr/opt/metrics-{id_cam}/throughput")
            id_cam += 1
    return paths


def format_value(values):
    new_val = []
    for val in values:
        new_val.append(float("{:.2f}".format(val)))
    return new_val


def check_all_nodes(scenario, producers_cloud, gateways_cloud, producers_hybrid, gateways_hybrid):
    print(f"Scenario [{scenario}], "
          f"producers_cloud [{len(producers_cloud)}], gateways_cloud [{len(gateways_cloud)}], "
          f"producers_hybrid [{len(producers_hybrid)}], gateways_hybrid [{len(gateways_hybrid)}].")


def multiply_arr(item_s, item_m, item_l, by_s, by_m, by_l):
    return np.multiply([item_s, item_m, item_l], [by_s, by_m, by_l])


# print("all_prod = ", all_prod)
# print("all_recv = ", all_recv)
# print("all_send = ", all_send)

# CSV FILES
# SMALL
cloud_producers_id_s = [8]
cloud_gateways_id_s = [79]
hybrid_producers_id_s = [8]
hybrid_gateways_id_s = [79]
producers_th_cloud_s1_csv_files = gen_path_to_files_pd(cloud_producers_id_s, 'cloud_s1', '20200524-124246', cluster, site)
gateways_th_cloud_s1_csv_files = gen_path_to_files_gw(cloud_gateways_id_s, 'cloud_s1', '20200524-124246', cluster, site)
producers_th_hybrid_s1_csv_files = gen_path_to_files_pd(hybrid_producers_id_s, 'hybrid_s1', '20200524-112210', cluster, site)
gateways_th_hybrid_s1_csv_files = gen_path_to_files_gw(hybrid_gateways_id_s, 'hybrid_s1', '20200524-112210', cluster, site)
# MEDIUM
cloud_producers_id_m = [8, 77, 78, 79]
cloud_gateways_id_m = [73, 74, 75, 76]  # FIXME add all gateways
hybrid_producers_id_m = [8, 77, 78, 79]
hybrid_gateways_id_m = [73, 74, 75, 76]  # FIXME add all gateways
producers_th_cloud_m1_csv_files = gen_path_to_files_pd(cloud_producers_id_m, 'cloud_m1', '20200524-025852', cluster, site)
gateways_th_cloud_m1_csv_files = gen_path_to_files_gw(cloud_gateways_id_m, 'cloud_m1', '20200524-025852', cluster, site)
producers_th_hybrid_m1_csv_files = gen_path_to_files_pd(hybrid_producers_id_m, 'hybrid_m1', '20200524-013620', cluster, site)
gateways_th_hybrid_m1_csv_files = gen_path_to_files_gw(hybrid_gateways_id_m, 'hybrid_m1', '20200524-013620', cluster, site)
# LARGE
cloud_producers_id_l = [65, 74, 75, 76, 77, 78, 79, 8, 66, 67, 69, 7, 70, 71, 72, 73]
cloud_gateways_id_l = [5, 58, 59, 6, 60, 61, 62, 64, 50, 51, 52, 53, 54, 55, 56, 57]  # FIXME add all gateways
hybrid_producers_id_l = [65, 74, 75, 76, 77, 78, 79, 8, 66, 67, 69, 7, 70, 71, 72, 73]
hybrid_gateways_id_l = [5, 58, 59, 6, 60, 61, 62, 64, 50, 51, 52, 53, 54, 55, 56, 57]  # FIXME add all gateways
producers_th_cloud_l1_csv_files = gen_path_to_files_pd(cloud_producers_id_l, 'cloud_l1', '20200523-170223', cluster, site)
gateways_th_cloud_l1_csv_files = gen_path_to_files_gw(cloud_gateways_id_l, 'cloud_l1', '20200523-170223', cluster, site)
producers_th_hybrid_l1_csv_files = gen_path_to_files_pd(hybrid_producers_id_l, 'hybrid_l1', '20200523-154113', cluster, site)
gateways_th_hybrid_l1_csv_files = gen_path_to_files_gw(hybrid_gateways_id_l, 'hybrid_l1', '20200523-154113', cluster, site)


# RAW DATA
# SMALL
producers_th_cloud_s1_arr = np.array(from_csv_to_arr(producers_th_cloud_s1_csv_files))  # * NUM_PRODUCERS
gateways_th_cloud_s1_arr = np.array(from_csv_to_arr(gateways_th_cloud_s1_csv_files))
producers_th_hybrid_s1_arr = np.array(from_csv_to_arr(producers_th_hybrid_s1_csv_files))  # * NUM_PRODUCERS
gateways_th_hybrid_s1_arr = np.array(from_csv_to_arr(gateways_th_hybrid_s1_csv_files))
check_all_nodes('SMALL', producers_th_cloud_s1_arr, gateways_th_cloud_s1_arr, producers_th_hybrid_s1_arr, gateways_th_hybrid_s1_arr)
# MEDIUM
producers_th_cloud_m1_arr = np.array(from_csv_to_arr(producers_th_cloud_m1_csv_files))  # * NUM_PRODUCERS
gateways_th_cloud_m1_arr = np.array(from_csv_to_arr(gateways_th_cloud_m1_csv_files))
producers_th_hybrid_m1_arr = np.array(from_csv_to_arr(producers_th_hybrid_m1_csv_files))  # * NUM_PRODUCERS
gateways_th_hybrid_m1_arr = np.array(from_csv_to_arr(gateways_th_hybrid_m1_csv_files))
check_all_nodes('MEDIUM', producers_th_cloud_m1_arr, gateways_th_cloud_m1_arr, producers_th_hybrid_m1_arr, gateways_th_hybrid_m1_arr)
# LARGE
producers_th_cloud_l1_arr = np.array(from_csv_to_arr(producers_th_cloud_l1_csv_files))  # * NUM_PRODUCERS
gateways_th_cloud_l1_arr = np.array(from_csv_to_arr(gateways_th_cloud_l1_csv_files))
producers_th_hybrid_l1_arr = np.array(from_csv_to_arr(producers_th_hybrid_l1_csv_files))  # * NUM_PRODUCERS
gateways_th_hybrid_l1_arr = np.array(from_csv_to_arr(gateways_th_hybrid_l1_csv_files))
check_all_nodes('LARGE', producers_th_cloud_l1_arr, gateways_th_cloud_l1_arr, producers_th_hybrid_l1_arr, gateways_th_hybrid_l1_arr)



# # AVERAGE
# SMALL
producers_th_cloud_s1_mean = np.mean(producers_th_cloud_s1_arr)
gateways_th_cloud_s1_mean = np.mean(gateways_th_cloud_s1_arr)
producers_th_hybrid_s1_mean = np.mean(producers_th_hybrid_s1_arr)
gateways_th_hybrid_s1_mean = np.mean(gateways_th_hybrid_s1_arr)
# MEDIUM
producers_th_cloud_m1_mean = np.mean(producers_th_cloud_m1_arr)
gateways_th_cloud_m1_mean = np.mean(gateways_th_cloud_m1_arr)
producers_th_hybrid_m1_mean = np.mean(producers_th_hybrid_m1_arr)
gateways_th_hybrid_m1_mean = np.mean(gateways_th_hybrid_m1_arr)
# LARGE
producers_th_cloud_l1_mean = np.mean(producers_th_cloud_l1_arr)
gateways_th_cloud_l1_mean = np.mean(gateways_th_cloud_l1_arr)
producers_th_hybrid_l1_mean = np.mean(producers_th_hybrid_l1_arr)
gateways_th_hybrid_l1_mean = np.mean(gateways_th_hybrid_l1_arr)


# # STANDARD DEVIATION
# SMALL
producers_th_cloud_s1_std = np.std(producers_th_cloud_s1_arr)
gateways_th_cloud_s1_std = np.std(gateways_th_cloud_s1_arr)
producers_th_hybrid_s1_std = np.std(producers_th_hybrid_s1_arr)
gateways_th_hybrid_s1_std = np.std(gateways_th_hybrid_s1_arr)
# MEDIUM
producers_th_cloud_m1_std = np.std(producers_th_cloud_m1_arr)
gateways_th_cloud_m1_std = np.std(gateways_th_cloud_m1_arr)
producers_th_hybrid_m1_std = np.std(producers_th_hybrid_m1_arr)
gateways_th_hybrid_m1_std = np.std(gateways_th_hybrid_m1_arr)
# LARGE
producers_th_cloud_l1_std = np.std(producers_th_cloud_l1_arr)
gateways_th_cloud_l1_std = np.std(gateways_th_cloud_l1_arr)
producers_th_hybrid_l1_std = np.std(producers_th_hybrid_l1_arr)
gateways_th_hybrid_l1_std = np.std(gateways_th_hybrid_l1_arr)


# CTEs_c_pd = [producers_th_cloud_s1_mean, producers_th_cloud_m1_mean, producers_th_cloud_l1_mean]
# CTEs_c_gw = [gateways_th_cloud_s1_mean, gateways_th_cloud_m1_mean, gateways_th_cloud_l1_mean]
# CTEs_h_pd = [producers_th_hybrid_s1_mean, producers_th_hybrid_m1_mean, producers_th_hybrid_l1_mean]
# CTEs_h_gw = [gateways_th_hybrid_s1_mean, gateways_th_hybrid_m1_mean, gateways_th_hybrid_l1_mean]

# CTEs_c_pd = multiply_arr(producers_th_cloud_s1_mean, producers_th_cloud_m1_mean, producers_th_cloud_l1_mean,
#                          len(producers_th_cloud_s1_arr), len(producers_th_cloud_m1_arr), len(producers_th_cloud_l1_arr))
CTEs_c_gw = multiply_arr(gateways_th_cloud_s1_mean, gateways_th_cloud_m1_mean, gateways_th_cloud_l1_mean,
                         len(gateways_th_cloud_s1_arr), len(gateways_th_cloud_m1_arr), len(gateways_th_cloud_l1_arr))
# CTEs_h_pd = multiply_arr(producers_th_hybrid_s1_mean, producers_th_hybrid_m1_mean, producers_th_hybrid_l1_mean,
#                          len(producers_th_hybrid_s1_arr), len(producers_th_hybrid_m1_arr), len(producers_th_hybrid_l1_arr))
CTEs_h_gw = multiply_arr(gateways_th_hybrid_s1_mean, gateways_th_hybrid_m1_mean, gateways_th_hybrid_l1_mean,
                         len(gateways_th_hybrid_s1_arr), len(gateways_th_hybrid_m1_arr), len(gateways_th_hybrid_l1_arr))


# error_c_pd = [producers_th_cloud_s1_std, producers_th_cloud_m1_std, producers_th_cloud_l1_std]
# error_c_gw = [gateways_th_cloud_s1_std, gateways_th_cloud_m1_std, gateways_th_cloud_l1_std]
# error_h_pd = [producers_th_hybrid_s1_std, producers_th_hybrid_m1_std, producers_th_hybrid_l1_std]
# error_h_gw = [gateways_th_hybrid_s1_std, gateways_th_hybrid_m1_std, gateways_th_hybrid_l1_std]

error_c_pd = multiply_arr(producers_th_cloud_s1_std, producers_th_cloud_m1_std, producers_th_cloud_l1_std,
                         len(producers_th_cloud_s1_arr), len(producers_th_cloud_m1_arr), len(producers_th_cloud_l1_arr))
error_c_gw = multiply_arr(gateways_th_cloud_s1_std, gateways_th_cloud_m1_std, gateways_th_cloud_l1_std,
                         len(gateways_th_cloud_s1_arr), len(gateways_th_cloud_m1_arr), len(gateways_th_cloud_l1_arr))
error_h_pd = multiply_arr(producers_th_hybrid_s1_std, producers_th_hybrid_m1_std, producers_th_hybrid_l1_std,
                         len(producers_th_hybrid_s1_arr), len(producers_th_hybrid_m1_arr), len(producers_th_hybrid_l1_arr))
error_h_gw = multiply_arr(gateways_th_hybrid_s1_std, gateways_th_hybrid_m1_std, gateways_th_hybrid_l1_std,
                         len(gateways_th_hybrid_s1_arr), len(gateways_th_hybrid_m1_arr), len(gateways_th_hybrid_l1_arr))


# print(CTEs_c_pd)
print(CTEs_c_gw)
# print(CTEs_h_pd)
print(CTEs_h_gw)

print(error_c_pd)
print(error_c_gw)
print(error_h_pd)
print(error_h_gw)

_color_data = 'darkseagreen'
_color_hybrid = 'slategray'  #[0.2, 0.4, 0.6, 0.6]
_color_cloud = 'lightsteelblue'

scenarios = ['Small', 'Medium', 'Large']
ind = np.arange(len(scenarios))*1
width = 0.2
fig, ax = plt.subplots()
# rects1 = ax.bar(ind - 0.3, format_value(CTEs_c_pd), width=width, color=_color_data, yerr=error_c_pd)
# rects2 = ax.bar(ind - 0.1, format_value(CTEs_c_gw), width=width, color=_color_cloud, yerr=error_c_gw)
# rects3 = ax.bar(ind + 0.1, format_value(CTEs_h_pd), width=width, color=_color_data, yerr=error_h_pd)
# rects4 = ax.bar(ind + 0.3, format_value(CTEs_h_gw), width=width, color=_color_hybrid, yerr=error_h_gw)

rects2 = ax.bar(ind - 0.1, format_value(CTEs_c_gw), width=width, color=_color_cloud, yerr=error_c_gw)
rects4 = ax.bar(ind + 0.1, format_value(CTEs_h_gw), width=width, color=_color_hybrid, yerr=error_h_gw)

# draw temporary red and blue lines and use them to create a legend
# line1 = plt.plot([], c=_color_data, label='Generated data')
line2 = plt.plot([], c=_color_cloud, label='Cloud-centric processing')
line3 = plt.plot([], c=_color_hybrid, label='Hybrid processing')

# red_patch = mpatches.Patch(color='red', label='The red data')
# plt.legend()
# ax.set_ylim([0,9000])
ax.set_ylabel('Amount of data (MB)')
ax.set_title('Amount of Data Sent to Cloud (e-f/f-c: 150mbit/1gbit)')
ax.set_xticks(ind)
ax.set_xticklabels(scenarios)
ax.legend()
ax.yaxis.grid(True)
autolabel(rects2, "left")
autolabel(rects4, "right")
fig.tight_layout()
plt.savefig(f"{plots_path}/tests-new/cost-data-150mbit-1gbit.png")
plt.show()

