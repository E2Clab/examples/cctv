import csv
import matplotlib.pyplot as plt
import pandas as pd

MEM = 0
CPU = 4
RECV = 9
SEND = 10

def get_len(*y_axes):
    lens = []
    for v in y_axes:
        print(">>>>>>>>> y_axes = ", len(v))
        lens.append(len(v))
    return min(lens)


def get_xy_values(csv_file, dstat_rec):
    x = []
    y = []
    with open(csv_file, 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')
        iter_plots = iter(plots)
        idx = 1
        for sk in range(6):
            next(iter_plots)
        for row in iter_plots:
            x.append(idx)
            y.append(int(row[dstat_rec]))
            idx+=1
    # print("y = ", y)
    return x, y


# FLINK
hybrid_1_dstat_flink = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-152008-hybrid_1/dstat.csv"
hybrid_5_dstat_flink = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_5/20200512-153705/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-153705-hybrid_5/dstat.csv"
hybrid_9_dstat_flink = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_9/20200512-155341/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-155341-hybrid_9/dstat.csv"
hybrid_13_dstat_flink = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_13/20200512-161011/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-161011-hybrid_13/dstat.csv"

#DESIGN
plots_path = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/plots"
plt.figure(figsize=(6, 3))
_fontsize = 10
_linecolor_blue = "blue"
_linecolor_red = "red"
_linecolor_purple = "purple"
_linecolor_orange = "orange"
my_colors = [_linecolor_blue, _linecolor_red, _linecolor_purple, _linecolor_orange]
plt.xticks(fontsize=_fontsize)
plt.yticks(fontsize=_fontsize)

# DSTAT TITLES
_title_dstat_mem = 'Memory'
_title_dstat_cpu = 'CPU'
_title_dstat_net = 'Network'
x_label_dstat = 'seconds'
y_label_dstat = 'Bytes'


#### FLINK MEM MULTIPLOT - HYBRID
x, y_h1 = get_xy_values(hybrid_1_dstat_flink, MEM)
x, y_h5 = get_xy_values(hybrid_5_dstat_flink, MEM)
x, y_h9 = get_xy_values(hybrid_9_dstat_flink, MEM)
x, y_h13 = get_xy_values(hybrid_13_dstat_flink, MEM)
max_len = get_len(y_h1, y_h5, y_h9, y_h13)
print("max_len =", max_len)
hybrid_1_29_dstat_flink_mem_dataset = {'x': [], 'hybrid_1': [], 'hybrid_5': [], 'hybrid_9': [], 'hybrid_13': []}
hybrid_1_29_dstat_flink_mem_dataset['x'] = x[:max_len]
hybrid_1_29_dstat_flink_mem_dataset['hybrid_1'] = y_h1[:max_len]
hybrid_1_29_dstat_flink_mem_dataset['hybrid_5'] = y_h5[:max_len]
hybrid_1_29_dstat_flink_mem_dataset['hybrid_9'] = y_h9[:max_len]
hybrid_1_29_dstat_flink_mem_dataset['hybrid_13'] = y_h13[:max_len]

for k, v in hybrid_1_29_dstat_flink_mem_dataset.items():
    print(f"{k} = {len(v)}")

dataframe = pd.DataFrame(hybrid_1_29_dstat_flink_mem_dataset)
print(dataframe)
plt.plot('x', 'hybrid_1', data=dataframe, color=_linecolor_blue, linewidth=1)
plt.plot('x', 'hybrid_5', data=dataframe, color=_linecolor_red, linewidth=1)
plt.plot('x', 'hybrid_9', data=dataframe, color=_linecolor_purple, linewidth=1)
plt.plot('x', 'hybrid_13', data=dataframe, color=_linecolor_orange, linewidth=1)
plt.title(_title_dstat_mem)
plt.xlabel(x_label_dstat, fontsize=_fontsize)
plt.ylabel(y_label_dstat, fontsize=_fontsize)
plt.legend()
plt.savefig(f"{plots_path}/flink_mem_hybrid_1_29.png")

