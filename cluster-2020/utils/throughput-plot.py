import csv
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

plots_path = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/plots"
plot_file_name = "end-to-end-latency.png"
_title = "Gateway throughput"
x_label = 'latency (ms)'
y_label = 'messages/s'
plt.figure(figsize=(6, 3))
_fontsize = 10


def all_messages(values):
    new_values = []
    _latency = 1
    for qtd in values:
        for i in range(int(qtd)):
            new_values.append(_latency)
        _latency += 1
    print("new_values =", new_values)
    return new_values


def print_dict(id, _dict, h1, h2):
    print(f"{id} = {_dict}")
    print(f"{id} len[{h1}] = {len(_dict[h1])}")
    print(f"{id} len[{h2}] = {len(_dict[h2])}")


def check_dict(id, _dict):
    sum_l = 0
    sum_m = 0
    for lts in _dict["latency"]:
        sum_l += lts
    for msg in _dict["messages"]:
        sum_m += msg
    print(f"[check_dict]{id} - len[latency]={len(_dict['latency'])}, max[latency]={max(_dict['latency'])}, sum_m={sum_m}")


def from_csv_to_dict(csv_file_path, scn_id, type):
    f = open(csv_file_path, 'r')
    n_messages = 0
    time_in_ms = 0
    l_idx = 0
    for l in f.readlines():
        if l_idx == 0:
            n_messages = int(l.split(':')[1])
        else:
            time_in_ms = int(l.split(':')[1])
        l_idx += 1

    msgs_s = round(n_messages / (time_in_ms / 1000))
    # print(n_messages)
    # print(time_in_ms)
    # print(round(msgs_s))
    # new_dict_th['id'] = scn_id
    # new_dict_th['throughput'].append(msgs_s)
    return {'x': [scn_id], 'messages/s': [msgs_s], 'type': type}




def plot_data_bar_plot(_df, file_path):
    _df.plot.bar()
    plt.title(_title)
    plt.ylabel(x_label, fontsize=_fontsize)
    plt.savefig(file_path)


def plot_data_bar_plot(_df, file_path):
    _df.plot.bar()
    plt.title("Edge-to-Cloud Latency")
    plt.ylabel(y_label, fontsize=_fontsize)
    plt.savefig(file_path)


def calc_relative_freq_of_msgs(_dict):
    key = "messages"
    _dataframe = pd.DataFrame(data=_dict)
    dataframe_rel = _dataframe[key] / _dataframe[key].sum()
    _dataframe[key] = round(dataframe_rel * 100)
    print(f">>> dataframe[{key}].sum() = {int(_dataframe[key].sum())}")
    return _dataframe, int(_dataframe[key].sum())


def add_scenario_to_dataframe(scenarios, df):
    for scenario in scenarios:
        # df.insert(0, scenario['id'], scenario['throughput'], True)
        df.append(scenario, ignore_index=True)



hybrid_1_th = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/experiment-results/gateways/paravance-32.rennes.grid5000.fr/opt/metrics/throughput"
hybrid_2_th = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_2/20200513-123831/experiment-results/gateways/paravance-3.rennes.grid5000.fr/opt/metrics/throughput"

hybrid_3_th = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_3/20200514-003542/experiment-results/gateways/paravance-12.rennes.grid5000.fr/opt/metrics/throughput"
hybrid_4_th = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/experiment-results/gateways/paravance-26.rennes.grid5000.fr/opt/metrics/throughput"

cloud_1_th = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_1/20200512-180112/experiment-results/gateways/paravance-32.rennes.grid5000.fr/opt/metrics/throughput"
cloud_2_th = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_2/20200513-150450/experiment-results/gateways/paravance-3.rennes.grid5000.fr/opt/metrics/throughput"

cloud_3_th = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_3/20200514-042418/experiment-results/gateways/paravance-12.rennes.grid5000.fr/opt/metrics/throughput"
cloud_4_th = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_4/20200513-051923/experiment-results/gateways/paravance-16.rennes.grid5000.fr/opt/metrics/throughput"

# index = ["4GW/160CAM", "8GW/320CAM", "12GW/480CAM"]
# data = {"Hybrid": [39, 38, 41],
#         "Cloud": [42, 43, 44]
#         }
# dataFrame = pd.DataFrame(data=data, index=index)
# dataFrame.plot.bar(rot=15, title="Gateway Throughput")
# plt.show(block=True)

# df = pd.DataFrame(data={})
# from_csv_to_dict(hybrid_1_th, 'hybrid_1', df)
# from_csv_to_dict(cloud_1_th, 'cloud_1', df)
# from_csv_to_dict(hybrid_2_th, 'hybrid_2', df)
# from_csv_to_dict(cloud_2_th, 'cloud_2', df)
# print(df)
# plot_data_bar_plot(df, f"{plots_path}/tests/th-{plot_file_name}")



# df1=pd.DataFrame({'x':[1,2,3,4,5],'y':[10,2,454,121,34]})
# df2=pd.DataFrame({'x':[4,1,2,5,3],'y':[54,12,65,12,8]})
# df1['hue']=1
# df2['hue']=2
# res=pd.concat([df1,df2])
# sns.barplot(x='x', y='y', data=res, hue='hue')
# plt.show()

_proc_type = ['Hybrid', 'Cloud']
_scenarios = ["4GWs/160CAMs", "8GWs/320CAMs", "12GWs/480CAMs", "16GWs/640CAMs"]
scenarios_dict = []
# {'x': ['a'], 'y': [23]}

scenarios_dict.append(from_csv_to_dict(hybrid_1_th, _scenarios[0], _proc_type[0]))
scenarios_dict.append(from_csv_to_dict(cloud_1_th, _scenarios[0], _proc_type[1]))
scenarios_dict.append(from_csv_to_dict(hybrid_2_th, _scenarios[1], _proc_type[0]))
scenarios_dict.append(from_csv_to_dict(cloud_2_th, _scenarios[1], _proc_type[1]))
scenarios_dict.append(from_csv_to_dict(hybrid_3_th, _scenarios[2], _proc_type[0]))
scenarios_dict.append(from_csv_to_dict(cloud_3_th, _scenarios[2], _proc_type[1]))
scenarios_dict.append(from_csv_to_dict(hybrid_4_th, _scenarios[3], _proc_type[0]))
scenarios_dict.append(from_csv_to_dict(cloud_4_th, _scenarios[3], _proc_type[1]))

_legend = ''
df_arr = []
for scn in scenarios_dict:
    _type = scn['type']
    del scn['type']
    _df = pd.DataFrame(scn)
    _df[_legend] = _type
    df_arr.append(_df)

res = pd.concat(df_arr)


# df1_1=pd.DataFrame({'x':[1],'y':[10]})
# df1_2=pd.DataFrame({'x':[2],'y':[2]})
# df2_1=pd.DataFrame({'x':[1],'y':[65]})
# df2_2=pd.DataFrame({'x':[2],'y':[12]})
# df1_1['hue']=1
# df1_2['hue']=1
# df2_1['hue']=2
# df2_2['hue']=2
# res=pd.concat([df1_1,df1_2, df2_1, df2_2])
print("res = ", len(res))
# https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/bar_stacked.html#sphx-glr-gallery-lines-bars-and-markers-bar-stacked-py
chart = sns.barplot(x='x', y='messages/s', data=res, hue=_legend, yerr=[[1,2],[3,4],[1,2],[3,4],[1,2],[3,4],[1,2],[3,4]])
chart.set_xticklabels(chart.get_xticklabels(), rotation=5)
# plt.legend(bbox_to_anchor=(.90, 1.18), loc=9, borderaxespad=0.)
plt.ylim(36, 48)
plt.title(_title)
# plt.show()
plt.savefig(f"{plots_path}/tests/gw-throughput.png")
