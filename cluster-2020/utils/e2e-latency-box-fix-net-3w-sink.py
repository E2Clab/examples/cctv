import csv
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plots_path = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/plots"
plot_file_name = "end-to-end-latency.png"


def all_messages(values):
    new_values = []
    _latency = 1
    for qtd in values:
        for i in range(int(qtd)):
            new_values.append(_latency)
        _latency += 1
    print("new_values =", new_values)
    return new_values


def print_dict(id, _dict, h1, h2):
    print(f"{id} = {_dict}")
    print(f"{id} len[{h1}] = {len(_dict[h1])}")
    print(f"{id} len[{h2}] = {len(_dict[h2])}")


def check_dict(id, _dict):
    sum_l = 0
    sum_m = 0
    for lts in _dict["latency"]:
        sum_l += lts
    for msg in _dict["messages"]:
        sum_m += msg
    print(f"[check_dict]{id} - len[latency]={len(_dict['latency'])}, max[latency]={max(_dict['latency'])}, sum_m={sum_m}")


def from_csv_to_dict(csv_file_path, scn_id):
    f = open(csv_file_path, "r")
    file_data = f.read()
    csv_values = iter(file_data.split(' '))
    # next(csv_values)  # skip label
    next(csv_values)
    latency_values = []
    message_values = []
    # idx_values = 1
    for _data in csv_values:
        # if idx_values > 2:
        #     break
        d = _data.split(',')
        latency_values.append(int(d[0]))
        message_values.append(int(d[1]))
        # idx_values += 1

    latency_dict = {"latency": latency_values, "messages": message_values}
    # print_dict('source', latency_dict, "latency", "messages")

    all_latency_arr = []
    for idx in range(len(message_values)):
        for num_of_msgs in range(message_values[idx]):
            all_latency_arr.append(latency_values[idx])

    print('all_latency_arr = ', all_latency_arr)
    print(f"VALIDATE: sum(message_values) = {sum(message_values)} == len(all_latency_arr) = {len(all_latency_arr)}, "
          f"MAX = {max(all_latency_arr)}, MIN = {min(all_latency_arr)}, MEAN = {sum(all_latency_arr)/len(all_latency_arr)}")
    return all_latency_arr


def calc_relative_freq_of_msgs(_dict):
    key = "messages"
    _dataframe = pd.DataFrame(data=_dict)
    dataframe_rel = _dataframe[key] / _dataframe[key].sum()
    _dataframe[key] = round(dataframe_rel * 100)
    print(f">>> dataframe[{key}].sum() = {int(_dataframe[key].sum())}")
    return _dataframe, int(_dataframe[key].sum())


# def add_scenario_to_dataframe(scenarios, df):
#     min_len = 100
#     _rel_freqs = []
#
#     for scenario in scenarios:
#         _rel_freq, _size = calc_relative_freq_of_msgs(scenario)
#         _rel_freqs.append({'id': scenario['id'], 'rf': _rel_freq})
#         min_len = _size if min_len > _size else min_len
#     print("selected min_len = ", min_len)
#
#     for _rel_freq in _rel_freqs:
#         # print(_rel_freq)  # print dataframe
#         all_msgs = all_messages(_rel_freq['rf']['messages'])
#         print(f">>> len of all_msgs = {len(all_msgs)}")
#         df.insert(0, _rel_freq['id'], all_msgs[:min_len], True)


def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

# set_box_color(bpl, '#D7191C') # colors are from http://colorbrewer2.org/

# ########################################## NEW
# Figure 2: 22 Mb 1 Gb / 3 Workloads
# 22Mb, 1Gb
# SMALL
hybrid_s2_e2e_s = "/home/drosendo/git/e2clab/e2clab/examples/cctv/scenarios/hybrid_s0/20200526-200606/experiment-results/sinks/out-sink/econome-4.nantes.grid5000.fr/opt/metrics/out-sink/latency"
cloud_s2_e2e_s = "/home/drosendo/git/e2clab/e2clab/examples/cctv/scenarios/cloud_s0/20200526-202434/experiment-results/sinks/out-sink/econome-4.nantes.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_s2_e2e_s_dict = from_csv_to_dict(hybrid_s2_e2e_s, 'hybrid_s2')
cloud_s2_e2e_s_dict = from_csv_to_dict(cloud_s2_e2e_s, 'cloud_s2')
# MEDIUM
hybrid_m2_e2e_m = "/home/drosendo/Documents/plots-pd/hybrid_s2_10gbit_10gbit"
cloud_m2_e2e_m = "/home/drosendo/Documents/plots-pd/cloud_s2_10gbit_10gbit"
hybrid_m2_e2e_m_dict = from_csv_to_dict(hybrid_m2_e2e_m, 'hybrid_m2')
cloud_m2_e2e_m_dict = from_csv_to_dict(cloud_m2_e2e_m, 'cloud_m2')
# LARGE
# hybrid_l2_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l2/20200523-155454/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"  # "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l2/20200523-154810/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_l2_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l2/20200523-171549/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency" # "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l2/20200523-170909/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
# hybrid_l2_e2e_l_dict = from_csv_to_dict(hybrid_l2_e2e_l, 'hybrid_l2')
# cloud_l2_e2e_l_dict = from_csv_to_dict(cloud_l2_e2e_l, 'cloud_l2')


scenarios = [hybrid_s2_e2e_s_dict, cloud_s2_e2e_s_dict,
             hybrid_m2_e2e_m_dict, cloud_m2_e2e_m_dict]#,
             # hybrid_l2_e2e_l_dict, cloud_l2_e2e_l_dict]
df = pd.DataFrame(data={})
# add_scenario_to_dataframe(scenarios, df)
print(df)

data1 = [scenarios[0], scenarios[1]]
data2 = [scenarios[2], scenarios[3]]
# data3 = [scenarios[4], scenarios[5]]

fig, ax = plt.subplots()

# labels1 = ['22mbit, 1gbit', '22mbit, 1gbit']
# labels2 = ['1gbit, 1gbit', '1gbit, 1gbit']
# labels3 = ['1gbit, 22mbit', '1gbit, 22mbit']
new_lbl = ['Small', 'Medium', 'Large']

medianprops = dict(linestyle='-', linewidth=1.5, color='black')
bplot1 = ax.boxplot(data1,
                    positions=[-0.4, 0.4], sym='',
                    widths=0.4,
                    vert=True,  # vertical box alignment
                    patch_artist=True,  # fill with color
                    labels=None,
                    medianprops=medianprops)  # will be used to label x-ticks

# bplot2 = ax.boxplot(data2,
#                     positions=[1.6, 2.4], sym='',
#                     widths=0.4, vert=True, patch_artist=True, labels=None, medianprops=medianprops)

# bplot3 = ax.boxplot(data3,
#                     positions=[3.6, 4.4],
#                     widths=0.4, vert=True, patch_artist=True, labels=None, medianprops=medianprops)


# fill with colors
_color_a = 'slategray'
_color_b = 'lightsteelblue'
colors = [_color_a, _color_b]
# for bplot in (bplot1, bplot2):
#     for patch, color in zip(bplot['boxes'], colors):
#         patch.set_facecolor(color)


plt.xticks(range(0, len(new_lbl) * 2, 2), new_lbl)
ax.set_title('End-to-end Latency (22mbit-1gbit)')
ax.legend([bplot1["boxes"][0], bplot1["boxes"][1]], ['Hybrid', 'Cloud'], loc="upper left", bbox_transform=fig.transFigure)
fig.tight_layout()
plt.savefig(f"{plots_path}/tests-new/e2e-l-figure-2-SML-1-Network-22-1.png")
plt.show()
