import csv
import pandas as pd
import matplotlib.pyplot as plt


plots_path = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/plots"
plot_file_name = "end-to-end-latency.png"
_title = "End-to-end Latency"
x_label = 'latency'
y_label = 'messages'
plt.figure(figsize=(6, 3))
_fontsize = 10


def print_dict(_dict, h1, h2):
    print(_dict)
    print(len(_dict[h1]))
    print(len(_dict[h2]))


def check_dict(_dict):
    sum_l = 0
    sum_m = 0
    for lts in _dict["latency"]:
        sum_l += lts
    for msg in _dict["messages"]:
        sum_m += msg

    print("len = ", len(_dict["latency"]))
    print("max = ", max(_dict["latency"]))
    print("sum_l = ", sum_l)
    print("sum_m = ", sum_m)


def from_csv_to_dict(csv_file_path):
    f = open(csv_file_path, "r")
    file_data = f.read()
    csv_values = iter(file_data.split(' '))
    next(csv_values)
    next(csv_values)
    latency_values = []
    message_values = []
    for _data in csv_values:
        d = _data.split(',')
        latency_values.append(int(d[0]))
        message_values.append(int(d[1]))

    latency_dict = {"latency": latency_values, "messages": message_values}
    print_dict(latency_dict, "latency", "messages")

    all_lat = max(latency_dict["latency"])
    all_latency_dict = {"latency": [], "messages": []}

    print("all_lat = ", all_lat)
    idx_lat = 1
    for lat in range(all_lat):
        all_latency_dict["latency"].append(idx_lat)
        all_latency_dict["messages"].append(0)
        idx_lat += 1

    for lat in all_latency_dict["latency"]:
        if lat in latency_dict["latency"]:
            all_latency_dict["messages"][lat - 1] = latency_dict["messages"][latency_dict["latency"].index(lat)]

    print_dict(all_latency_dict, "latency", "messages")
    check_dict(latency_dict)
    check_dict(all_latency_dict)
    return all_latency_dict

# E2C
hybrid_1_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/experiment-results/sinks/in-sink/paravance-4.rennes.grid5000.fr/opt/metrics/in-sink/latency"
hybrid_4_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/experiment-results/sinks/in-sink/paravance-56.rennes.grid5000.fr/opt/metrics/in-sink/latency"

# E2E
hybrid_1_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/experiment-results/sinks/out-sink/paravance-4.rennes.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_4_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/experiment-results/sinks/out-sink/paravance-56.rennes.grid5000.fr/opt/metrics/out-sink/latency"





dataframe = pd.DataFrame(data=from_csv_to_dict(hybrid_1_dstat_e2e))
dataframe.plot.bar(x="latency", y="messages")
plt.title(_title)
plt.xlabel(x_label, fontsize=_fontsize)
plt.ylabel(y_label, fontsize=_fontsize)
plt.legend()
plt.savefig(f"{plots_path}/{plot_file_name}")


#
# plt.plot('x', key, data=dataframe)
# plt.savefig(f"{plots_path}/{plot_file_name}")

# lts = []
# mgs = []
#
# file_name = 'cloud-1'
# f = open(f"/home/drosendo/Downloads/{file_name}", "r")
# file_data = f.read()
# values = iter(file_data.split(' '))
# next(values)
#
# for fq in values:
#     lts.append(int(fq.split(',')[0]))
#     mgs.append(int(fq.split(',')[1]))
#
# print("lts = ", max(lts))
# print("mgs = ", max(mgs))
#
# lts_zr = [0] * max(lts)
# print(lts_zr)
#
# with open(f'/home/drosendo/Downloads/new-{file_name}-zeros.csv', 'w', newline='') as file:
#     writer = csv.writer(file)
#     writer.writerow(["latency", "messages"])
#
#     for x in range(max(lts)):
#         if x not in lts:
#             writer.writerow([x, 0])
#         else:
#             writer.writerow([lts[lts.index(x)], mgs[lts.index(x)]])






# file_name = "test"
# with open(f'/home/drosendo/Downloads/new-{file_name}.csv', 'w', newline='') as file:
#     writer = csv.writer(file)
#     writer.writerow(["latency", "messages"])
#
#     for fq in values:
#         writer.writerow(fq.split(','))