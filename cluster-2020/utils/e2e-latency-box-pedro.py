import csv
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plots_path = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/plots"
plot_file_name = "end-to-end-latency.png"


def all_messages(values):
    new_values = []
    _latency = 1
    for qtd in values:
        for i in range(int(qtd)):
            new_values.append(_latency)
        _latency += 1
    print("new_values =", new_values)
    return new_values[:len(new_values)-1] # FIXME


def print_dict(id, _dict, h1, h2):
    print(f"{id} = {_dict}")
    print(f"{id} len[{h1}] = {len(_dict[h1])}")
    print(f"{id} len[{h2}] = {len(_dict[h2])}")


def check_dict(id, _dict):
    sum_l = 0
    sum_m = 0
    for lts in _dict["latency"]:
        sum_l += lts
    for msg in _dict["messages"]:
        sum_m += msg
    print(f"[check_dict]{id} - len[latency]={len(_dict['latency'])}, max[latency]={max(_dict['latency'])}, sum_m={sum_m}")


def from_csv_to_dict(csv_file_path, scn_id):
    f = open(csv_file_path, "r")
    file_data = f.read()
    csv_values = iter(file_data.split(' '))
    # next(csv_values) # FIXME remove msgs with latency 0
    next(csv_values)
    latency_values = []
    message_values = []
    # idx_values = 1
    for _data in csv_values:
        # if idx_values > 2:
        #     break
        d = _data.split(',')
        latency_values.append(int(d[0]))
        message_values.append(int(d[1]))
        # idx_values += 1

    latency_dict = {"latency": latency_values, "messages": message_values}
    print_dict('source', latency_dict, "latency", "messages")

    all_lat = max(latency_dict["latency"])
    all_latency_dict = {"id": scn_id, "latency": [], "messages": []}

    all_latency_dict = {"id": scn_id, "latency": [], "messages": []}
    all_latency_dict['latency'] = latency_dict['latency']
    all_latency_dict['messages'] = latency_dict['messages']

    # print("all_lat = ", all_lat)
    idx_lat = 1
    # for lat in range(all_lat):
    #     all_latency_dict["latency"].append(idx_lat)
    #     all_latency_dict["messages"].append(0)
    #     idx_lat += 1

    # for lat in all_latency_dict["latency"]:
    #     if lat in latency_dict["latency"]:
    #         all_latency_dict["messages"][lat - 1] = latency_dict["messages"][latency_dict["latency"].index(lat)]

    # print_dict('zeros', all_latency_dict, "latency", "messages")
    check_dict('source', latency_dict)
    # check_dict('zeros', all_latency_dict)
    return all_latency_dict


def calc_relative_freq_of_msgs(_dict):
    key = "messages"
    _dataframe = pd.DataFrame(data=_dict)
    dataframe_rel = _dataframe[key] / _dataframe[key].sum()
    _dataframe[key] = round(dataframe_rel * 100)
    print(f">>> dataframe[{key}].sum() = {int(_dataframe[key].sum())}")
    return _dataframe, int(_dataframe[key].sum())


def add_scenario_to_dataframe(scenarios, df):
    min_len = 100
    _rel_freqs = []

    for scenario in scenarios:
        _rel_freq, _size = calc_relative_freq_of_msgs(scenario)
        _rel_freqs.append({'id': scenario['id'], 'rf': _rel_freq})
        min_len = _size if min_len > _size else min_len
    print("selected min_len = ", min_len)

    for _rel_freq in _rel_freqs:
        # print(_rel_freq)  # print dataframe
        all_msgs = all_messages(_rel_freq['rf']['messages'])
        print(f">>> len of all_msgs = {len(all_msgs)}")
        df.insert(0, _rel_freq['id'], all_msgs[:min_len], True)


def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)


# def remove_outliers(_data):
#     _mean = np.array(_data).mean()
#     for _d in _data:
#         if _d > 4*_mean:
#             print(f"removing outlier = {_d}, mean[{_mean}]")
#             _data.remove(_d)


hybrid_s_10gbit_10gbit = "/home/drosendo/Documents/plots-pd/cloud_s2_10gbit_10gbit"
cloud_s2_10gbit_10gbit_dict = from_csv_to_dict(hybrid_s_10gbit_10gbit, 'cloud_s2_10gbit_10gbit')

# hybrid_s_22mbit_1gbit = "/home/drosendo/Documents/plots-pd/hybrid_s2_22mbit_1gbit"
# hybrid_s_22mbit_1gbit_dict = from_csv_to_dict(hybrid_s_22mbit_1gbit, 'hybrid_s_22mbit_1gbit')
#
# cloud_s_22mbit_1gbit = "/home/drosendo/Documents/plots-pd/cloud_s2_22mbit_1gbit"
# cloud_s_22mbit_1gbit_dict = from_csv_to_dict(cloud_s_22mbit_1gbit, 'cloud_s_22mbit_1gbit')


# scenarios = [hybrid_s_10gbit_10gbit_dict] # FIXME
scenarios = [cloud_s2_10gbit_10gbit_dict] #, cloud_s_22mbit_1gbit_dict]
df = pd.DataFrame(data={})
add_scenario_to_dataframe(scenarios, df)
print(df)

data1 = [df['cloud_s2_10gbit_10gbit']]
# data1 = [df['hybrid_s_10gbit_10gbit']]  # FIXME

fig, ax = plt.subplots()

medianprops = dict(linestyle='-', linewidth=1.5, color='black')
bplot1 = ax.boxplot(data1)  # will be used to label x-ticks

plt.xticks()
ax.set_title('End-to-end Latency (Small Scenario)')
fig.tight_layout()
plt.savefig(f"{plots_path}/tests-new/e2e-l-figure-1-L-3-Networks.png")
plt.show()
