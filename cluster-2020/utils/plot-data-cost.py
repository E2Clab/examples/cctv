import csv
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def get_xy_values(csv_file, dstat_rec):
    y = []
    with open(csv_file, 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')
        iter_plots = iter(plots)
        for sk in range(6):
            next(iter_plots)
        for row in iter_plots:
            y.append(float(row[dstat_rec]))
    y_cut = y[100:150]
    print("size = ", len(y_cut))
    return y_cut


plots_path = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/plots"
TO_MB = 1000000
TIMEOUT = 220
SCN_S = 1*TIMEOUT
SCN_M = 4*TIMEOUT
SCN_L = 16*TIMEOUT
all_prod = []
all_recv = []
all_send = []

cloud_s1_dstat_pd = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_s1/20200520-170441/dstat/parasilo-13.rennes.grid5000.fr/builds/dstat-20200520-170441-cloud_s1/dstat.csv"
cloud_s1_dstat_gw = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_s1/20200520-170441/dstat/parasilo-12.rennes.grid5000.fr/builds/dstat-20200520-170441-cloud_s1/dstat.csv"
cloud_s1_dstat_kafka = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_s1/20200520-170441/dstat/parasilo-10.rennes.grid5000.fr/builds/dstat-20200520-170441-cloud_s1/dstat.csv"
cloud_s1_dstat_flink = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_s1/20200520-171613/dstat/parasilo-1.rennes.grid5000.fr/builds/dstat-20200520-171613-cloud_s1/dstat.csv"
cloud_m1_dstat_pd = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_m1/20200521-122903/dstat/parasilo-5.rennes.grid5000.fr/builds/dstat-20200521-122903-cloud_m1/dstat.csv"
cloud_m1_dstat_gw = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_m1/20200521-122903/dstat/parasilo-15.rennes.grid5000.fr/builds/dstat-20200521-122903-cloud_m1/dstat.csv"
cloud_m1_dstat_kafka = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_m1/20200521-122903/dstat/parasilo-12.rennes.grid5000.fr/builds/dstat-20200521-122903-cloud_m1/dstat.csv"
cloud_m1_dstat_flink = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_m1/20200521-123533/dstat/parasilo-10.rennes.grid5000.fr/builds/dstat-20200521-123533-cloud_m1/dstat.csv"
cloud_l1_dstat_pd = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l1/20200520-213155/dstat/grisou-36.nancy.grid5000.fr/builds/dstat-20200520-213155-cloud_l1/dstat.csv"
cloud_l1_dstat_gw = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l1/20200520-213155/dstat/grisou-33.nancy.grid5000.fr/builds/dstat-20200520-213155-cloud_l1/dstat.csv"
cloud_l1_dstat_kafka = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l1/20200520-213155/dstat/grisou-16.nancy.grid5000.fr/builds/dstat-20200520-213155-cloud_l1/dstat.csv"
cloud_l1_dstat_flink = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l1/20200520-213914/dstat/grisou-1.nancy.grid5000.fr/builds/dstat-20200520-213914-cloud_l1/dstat.csv"

hybrid_s1_dstat_gw = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_s1/20200521-175653/dstat/parasilo-12.rennes.grid5000.fr/builds/dstat-20200521-175653-hybrid_s1/dstat.csv"
hybrid_s1_dstat_flink = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_s1/20200521-180246/dstat/parasilo-10.rennes.grid5000.fr/builds/dstat-20200521-180246-hybrid_s1/dstat.csv"
hybrid_m1_dstat_gw = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_m1/20200522-000635/dstat/gros-17.nancy.grid5000.fr/builds/dstat-20200522-000635-hybrid_m1/dstat.csv"
hybrid_m1_dstat_flink = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_m1/20200522-000635/dstat/gros-102.nancy.grid5000.fr/builds/dstat-20200522-000635-hybrid_m1/dstat.csv"
hybrid_l1_dstat_gw = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l1/20200521-164942/dstat/grisou-19.nancy.grid5000.fr/builds/dstat-20200521-164942-hybrid_l1/dstat.csv"
hybrid_l1_dstat_flink = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l1/20200521-165654/dstat/grisou-15.nancy.grid5000.fr/builds/dstat-20200521-165654-hybrid_l1/dstat.csv"


# CLOUD_S1
cloud_s1_dstat_pd_send = np.array(get_xy_values(cloud_s1_dstat_pd, 10))
cloud_s1_dstat_gw_recv = np.array(get_xy_values(cloud_s1_dstat_gw, 9))
cloud_s1_dstat_gw_send = np.array(get_xy_values(cloud_s1_dstat_gw, 10))
# cloud_s1_dstat_kafka_recv = np.array(get_xy_values(cloud_s1_dstat_kafka, 9))
# cloud_s1_dstat_flink_send = np.array(get_xy_values(cloud_s1_dstat_flink, 9))
all_prod.append((np.mean(cloud_s1_dstat_pd_send)/TO_MB)*SCN_S)
all_recv.append((np.mean(cloud_s1_dstat_gw_recv)/TO_MB)*SCN_S)
all_send.append((np.mean(cloud_s1_dstat_gw_send)/TO_MB)*SCN_S)  # FIXME


# CLOUD_M1
cloud_m1_dstat_pd_send = np.array(get_xy_values(cloud_m1_dstat_pd, 10))
cloud_m1_dstat_gw_recv = np.array(get_xy_values(cloud_m1_dstat_gw, 9))
cloud_m1_dstat_gw_send = np.array(get_xy_values(cloud_m1_dstat_gw, 10))
# cloud_m1_dstat_kafka_recv = np.array(get_xy_values(cloud_m1_dstat_kafka, 9))
# cloud_m1_dstat_flink_send = np.array(get_xy_values(cloud_m1_dstat_flink, 9))
all_prod.append((np.mean(cloud_m1_dstat_pd_send)/TO_MB)*SCN_S)
all_recv.append((np.mean(cloud_m1_dstat_gw_recv)/TO_MB)*SCN_M)
all_send.append((np.mean(cloud_m1_dstat_gw_send)/TO_MB)*SCN_M)  # FIXME Kafka recv


# CLOUD_L1
cloud_l1_dstat_pd_send = np.array(get_xy_values(cloud_l1_dstat_pd, 10))
cloud_l1_dstat_gw_recv = np.array(get_xy_values(cloud_l1_dstat_gw, 9))
cloud_l1_dstat_gw_send = np.array(get_xy_values(cloud_l1_dstat_gw, 10))
# cloud_l1_dstat_kafka_recv = np.array(get_xy_values(cloud_l1_dstat_kafka, 9))
# cloud_l1_dstat_flink_send = np.array(get_xy_values(cloud_l1_dstat_flink, 9))
all_prod.append((np.mean(cloud_l1_dstat_pd_send)/TO_MB)*SCN_S)
all_recv.append((np.mean(cloud_l1_dstat_gw_recv)/TO_MB)*SCN_L)
all_send.append((np.mean(cloud_l1_dstat_gw_send)/TO_MB)*SCN_L)  # FIXME Kafka recv

# HYBRID_S1
# hybrid_s1_dstat_gw_recv = np.array(get_xy_values(hybrid_s1_dstat_gw, 9))
# hybrid_s1_dstat_gw_send = np.array(get_xy_values(hybrid_s1_dstat_gw, 10))
# # hybrid_s1_dstat_flink_recv = np.array(get_xy_values(hybrid_s1_dstat_flink, 9))
# all_recv.append((np.mean(hybrid_s1_dstat_gw_recv)/TO_MB)*SCN_S)
# all_send.append((np.mean(hybrid_s1_dstat_gw_send)/TO_MB)*SCN_S)  # FIXME Kafka recv
# # HYBRID_M1
# hybrid_m1_dstat_gw_recv = np.array(get_xy_values(hybrid_m1_dstat_gw, 9))
# hybrid_m1_dstat_gw_send = np.array(get_xy_values(hybrid_m1_dstat_gw, 10))
# # hybrid_m1_dstat_flink_recv = np.array(get_xy_values(hybrid_m1_dstat_flink, 9))
# all_recv.append((np.mean(hybrid_m1_dstat_gw_recv)/TO_MB)*SCN_M)
# all_send.append((np.mean(hybrid_m1_dstat_gw_send)/TO_MB)*SCN_M)  # FIXME Kafka recv
# # HYBRID_L1
# hybrid_l1_dstat_gw_recv = np.array(get_xy_values(hybrid_l1_dstat_gw, 9))
# hybrid_l1_dstat_gw_send = np.array(get_xy_values(hybrid_l1_dstat_gw, 10))
# # hybrid_l1_dstat_flink_recv = np.array(get_xy_values(hybrid_l1_dstat_flink, 9))
# all_recv.append((np.mean(hybrid_l1_dstat_gw_recv)/TO_MB)*SCN_L)
# all_send.append((np.mean(hybrid_l1_dstat_gw_send)/TO_MB)*SCN_L)  # FIXME Kafka recv



print("all_prod = ", all_prod)
print("all_recv = ", all_recv)
print("all_send = ", all_send)

labels = ['C. Small', 'C. Medium', 'C. Large']#, 'H. Small', 'H. Medium', 'H. Large']
x = np.arange(len(labels))
width = 0.3
fig, ax = plt.subplots()
rects0 = ax.bar(x - width/3 - width, all_prod, width, label='Produced')
rects1 = ax.bar(x - width/3, all_recv, width, label='Recv in GW')
rects2 = ax.bar(x + width/3, all_send, width, label='Sent to Cloud')

ax.set_ylabel('Amount of data (MB)')
ax.set_title('Data generated vs sent to Cloud')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()
fig.tight_layout()
plt.savefig(f"{plots_path}/tests-new/cost-data.png")
plt.show()
