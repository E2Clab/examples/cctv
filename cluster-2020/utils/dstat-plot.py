import csv
import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path

MEM = 0
CPU = 4
RECV = 9
SEND = 10

def get_len(y_axes):
    lens = []

    for value in y_axes.values():
        lens.append(len(value))
    return min(lens)


def get_xy_values(csv_file, dstat_rec):
    y = []
    with open(csv_file, 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')
        iter_plots = iter(plots)
        for sk in range(6):
            next(iter_plots)
        for row in iter_plots:
            y.append(float(row[dstat_rec]))  # FIXME none float
    print("y = ", y)
    return y


plt.figure(figsize=(6, 3))
_linecolor_blue = "blue"
_linecolor_red = "red"
_linecolor_purple = "purple"
_linecolor_orange = "orange"
my_colors = [_linecolor_blue, _linecolor_red, _linecolor_purple, _linecolor_orange]

# DSTAT TITLES
_title_dstat_mem = 'Memory'
_title_dstat_cpu = 'CPU'
_title_dstat_net = 'Network'
x_label_dstat = 'seconds'
y_label_dstat_cpu = 'Usage (in %)'
y_label_dstat_mem = 'Gigabytes'
y_label_dstat_net = 'Bytes'


def gen_plot(raw_data, dstat_rec, _title, x_label, y_label, plots_path, plot_file_name):
    x_axe = []
    y_axes = {}
    _dataset_dict = {}
    _labels = []

    for key, value in raw_data.items():
        _labels.append(key)
        y_axes.update({key: get_xy_values(value, dstat_rec)})

    max_len = get_len(y_axes)

    x_axe = list(range(1, max_len+1))
    for key, value in y_axes.items():
        cut_arr = value[:max_len]
        y_axes.update({key: cut_arr})

    _dataset_dict.update({'x': x_axe})
    _dataset_dict.update(y_axes)
    print(_dataset_dict)
    dataframe = pd.DataFrame(_dataset_dict)

    if dstat_rec == 0:
        dataframe.iloc[:, 1:] = dataframe.iloc[:, 1:].div(1000000000)  # GB
    if dstat_rec == 9 or dstat_rec == 10:
        dataframe.iloc[:, 1:] = dataframe.iloc[:, 1:].div(1000000)  # MB

    print("x+x+", dataframe)
    idx_color = 0

    _color_hybrid = 'slategray'
    _color_cloud = 'lightsteelblue'
    linecolors = [_color_cloud]
    linestyles = ['-']
    linewidths = [1]
    if len(raw_data) == 2:
        linecolors = [_color_cloud, _color_hybrid]
        linestyles = ['-', ':']
        linewidths = [3, 3]
    elif len(raw_data) == 4:
        linecolors = [_color_cloud, _color_cloud, _color_hybrid, _color_hybrid]
        linestyles = ['-', '-', ':', ':']  # ['-', '--', '-.', ':']
        linewidths = [1, 3, 1, 3]

    fig, ax = plt.subplots()

    for aux_idx in range(len(raw_data)):
        ax.plot(dataframe[_labels[aux_idx]], linestyle=linestyles[aux_idx], linewidth=linewidths[aux_idx], color=linecolors[aux_idx])
        plt.plot([], c=linecolors[aux_idx], label=_labels[aux_idx], linestyle=linestyles[aux_idx], linewidth=linewidths[aux_idx])

    _fontsize = 12
    plt.rcParams.update({'font.size': _fontsize})
    plt.xticks(fontsize=_fontsize + 1)
    plt.yticks(fontsize=_fontsize + 1)
    plt.xlabel(x_label, fontsize=_fontsize)
    ax.set_ylabel(y_label, fontsize=_fontsize + 1)
    plt.title(_title, fontsize=_fontsize + 1)

    plt.legend()
    plt.savefig(f"{plots_path}/{plot_file_name}")

experiment_id = "20200729-094925"
root_dir = f"{str(Path.home())}/git/e2clab-examples/plantnet/getting_started/{experiment_id}"
site = "rennes"
cluster = "parasilo"
node_id = "13"
prefix = f"{root_dir}/dstat/{cluster}-{node_id}.{site}.grid5000.fr/builds"

hybrid_l1_dstat_gw = f"{prefix}/dstat-20200729-094925-getting_started/dstat.csv"

raw_data = {}
raw_data.update({'Identification Engine': hybrid_l1_dstat_gw})
gen_plot(raw_data, MEM, f"Gateway {_title_dstat_mem} Usage", x_label_dstat, 'GB', root_dir, "gw_mem_20_40_1gbit-1gbit.png")
gen_plot(raw_data, CPU, f"Gateway {_title_dstat_cpu} Usage", x_label_dstat, 'usage in %', root_dir, "gw_cpu_20_40_1gbit-1gbit.png")