import csv
# import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plots_path = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/plots"
plot_file_name = "end-to-end-latency.png"
_fontsize = 12

def all_messages(values):
    new_values = []
    _latency = 1
    for qtd in values:
        for i in range(int(qtd)):
            new_values.append(_latency)
        _latency += 1
    print("new_values =", new_values)
    return new_values


def print_dict(id, _dict, h1, h2):
    print(f"{id} = {_dict}")
    print(f"{id} len[{h1}] = {len(_dict[h1])}")
    print(f"{id} len[{h2}] = {len(_dict[h2])}")


def check_dict(id, _dict):
    sum_l = 0
    sum_m = 0
    for lts in _dict["latency"]:
        sum_l += lts
    for msg in _dict["messages"]:
        sum_m += msg
    print(f"[check_dict]{id} - len[latency]={len(_dict['latency'])}, max[latency]={max(_dict['latency'])}, sum_m={sum_m}")


def from_csv_to_dict(csv_file_path, scn_id):
    f = open(csv_file_path, "r")
    file_data = f.read()
    csv_values = iter(file_data.split(' '))
    # next(csv_values)  # skip label
    next(csv_values)
    latency_values = []
    message_values = []
    # idx_values = 1
    for _data in csv_values:
        # if idx_values > 2:
        #     break
        d = _data.split(',')
        latency_values.append(int(d[0]))
        message_values.append(int(d[1]))
        # idx_values += 1

    latency_dict = {"latency": latency_values, "messages": message_values}
    # print_dict('source', latency_dict, "latency", "messages")

    all_latency_arr = []
    for idx in range(len(message_values)):
        for num_of_msgs in range(message_values[idx]):
            all_latency_arr.append(latency_values[idx])

    print('all_latency_arr = ', all_latency_arr)
    print(f"VALIDATE: sum(message_values) = {sum(message_values)} == len(all_latency_arr) = {len(all_latency_arr)}, "
          f"MAX = {max(all_latency_arr)}, MIN = {min(all_latency_arr)}, MEAN = {sum(all_latency_arr)/len(all_latency_arr)}")
    return all_latency_arr


def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

# set_box_color(bpl, '#D7191C') # colors are from http://colorbrewer2.org/

# ########################################## NEW
# Figure 2: 22 Mb 1 Gb / 3 Workloads
# 22Mb, 1Gb
# SMALL
hybrid_s2_e2e_s = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_s2/20200527-035210/experiment-results/sinks/out-sink/econome-9.nantes.grid5000.fr/opt/metrics/out-sink/latency"
cloud_s2_e2e_s = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_s2/20200527-041046/experiment-results/sinks/out-sink/econome-9.nantes.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_s2_e2e_s_dict = from_csv_to_dict(hybrid_s2_e2e_s, 'hybrid_s2')
cloud_s2_e2e_s_dict = from_csv_to_dict(cloud_s2_e2e_s, 'cloud_s2')
# MEDIUM
hybrid_m2_e2e_m = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_m2/20200526-213334/experiment-results/sinks/out-sink/econome-8.nantes.grid5000.fr/opt/metrics/out-sink/latency"
cloud_m2_e2e_m = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_m2/20200526-223143/experiment-results/sinks/out-sink/econome-8.nantes.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_m2_e2e_m_dict = from_csv_to_dict(hybrid_m2_e2e_m, 'hybrid_m2')
cloud_m2_e2e_m_dict = from_csv_to_dict(cloud_m2_e2e_m, 'cloud_m2')
# LARGE
# hybrid_l2_e2e_l = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l2/20200527-095608/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
# cloud_l2_e2e_l = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l2/20200527-101113/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_l2_e2e_l = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l2/20200527-083852/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"
cloud_l2_e2e_l = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l2/20200527-100339/experiment-results/sinks/out-sink/grisou-9.nancy.grid5000.fr/opt/metrics/out-sink/latency"

hybrid_l2_e2e_l_dict = from_csv_to_dict(hybrid_l2_e2e_l, 'hybrid_l2')
cloud_l2_e2e_l_dict = from_csv_to_dict(cloud_l2_e2e_l, 'cloud_l2')


scenarios = [hybrid_s2_e2e_s_dict, cloud_s2_e2e_s_dict,
             hybrid_m2_e2e_m_dict, cloud_m2_e2e_m_dict,
             hybrid_l2_e2e_l_dict, cloud_l2_e2e_l_dict]
# df = pd.DataFrame(data={})
# add_scenario_to_dataframe(scenarios, df)
# print(df)

data1 = [scenarios[0], scenarios[1]]
data2 = [scenarios[2], scenarios[3]]
data3 = [scenarios[4], scenarios[5]]

fig, ax = plt.subplots()

# labels1 = ['22mbit, 1gbit', '22mbit, 1gbit']
# labels2 = ['1gbit, 1gbit', '1gbit, 1gbit']
# labels3 = ['1gbit, 22mbit', '1gbit, 22mbit']
new_lbl = ['Small', 'Medium', 'Large']

medianprops = dict(linestyle='-', linewidth=1.5, color='black')
bplot1 = ax.boxplot(data1,
                    positions=[-0.4, 0.4], sym='',
                    # positions=[1,2],
                    widths=0.4,
                    vert=True,  # vertical box alignment
                    patch_artist=True,  # fill with color
                    labels=None,
                    medianprops=medianprops)  # will be used to label x-ticks

bplot2 = ax.boxplot(data2,
                    positions=[1.6, 2.4], sym='',
                    # positions=[3,4],
                    widths=0.4, vert=True, patch_artist=True, labels=None, medianprops=medianprops)

bplot3 = ax.boxplot(data3,
                    # positions=np.array(range(len(data3)))*2.0-0.4, sym='',
                    positions=[3.6, 4.4],
                    widths=0.4, vert=True, patch_artist=True, labels=None, medianprops=medianprops)

# fill with colors

_color_a = 'slategray'
_color_b = 'lightsteelblue'
colors = [_color_a, _color_b]
for bplot in (bplot1, bplot2, bplot3):
    for patch, color in zip(bplot['boxes'], colors):
        patch.set_facecolor(color)

# draw temporary red and blue lines and use them to create a legend
# plt.plot([], c='#D7191C', label='Apples')
# plt.plot([], c='#2C7BB6', label='Oranges')
# plt.legend()
# Set the axes ranges and axes labels

# num_boxes = 6
# ax.set_xlim(0.5, num_boxes + 0.5)
# ax.set_xticklabels(labels1 + labels2 + labels3, rotation=15, fontsize=10)
# ax.set_xticklabels(new_lbl, rotation=15, fontsize=8)

plt.rcParams.update({'font.size': _fontsize})
ax.set_ylabel('Latency (in ms)', fontsize=_fontsize+1)
# ax.set_xlabel('Number of Cameras', fontsize=_fontsize+1)
ax.set_title('End-to-end Latency 22mbit (E-F) / 1gbit (F-C)', fontsize=_fontsize+1)
plt.xticks(fontsize=_fontsize+1)
plt.yticks(fontsize=_fontsize+1)


plt.xticks(range(0, len(new_lbl) * 2, 2), new_lbl)
# plt.xlim(-2, len(new_lbl)*2)


ax.legend([bplot1["boxes"][0], bplot1["boxes"][1]], ['Hybrid', 'Cloud'], loc="upper left", bbox_transform=fig.transFigure)
# plt.tick_params(axis='x', pad=3)
fig.tight_layout()
plt.savefig(f"{plots_path}/tests-new/e2e-l-figure-2-SML-1-Network-22-1.png")
plt.show()


# print(np.array(range(len(data3)))*2.0-0.4)
# print(np.array(range(len(data3)))*2.0+0.4)