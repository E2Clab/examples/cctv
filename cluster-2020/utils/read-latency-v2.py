import csv
import pandas as pd
import matplotlib.pyplot as plt


plots_path = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/plots"
plot_file_name = "end-to-end-latency.png"
_title = "End-to-end Latency"
x_label = 'latency'
y_label = 'messages (in %)'
plt.figure(figsize=(6, 3))
_fontsize = 10


def print_dict(_dict, h1, h2):
    print("[print_dict] ", _dict)
    print("[print_dict] ", len(_dict[h1]))
    print("[print_dict] ", len(_dict[h2]))


def check_dict(_dict):
    sum_l = 0
    sum_m = 0
    for lts in _dict["latency"]:
        sum_l += lts
    for msg in _dict["messages"]:
        sum_m += msg

    print("[check_dict] len = ", len(_dict["latency"]))
    print("[check_dict] max = ", max(_dict["latency"]))
    print("[check_dict] sum_l = ", sum_l)
    print("[check_dict] sum_m = ", sum_m)


def from_csv_to_dict(csv_file_path):
    f = open(csv_file_path, "r")
    file_data = f.read()
    csv_values = iter(file_data.split(' '))
    next(csv_values)
    next(csv_values)
    latency_values = []
    message_values = []
    for _data in csv_values:
        d = _data.split(',')
        latency_values.append(int(d[0]))
        message_values.append(int(d[1]))

    latency_dict = {"latency": latency_values, "messages": message_values}
    print_dict(latency_dict, "latency", "messages")

    all_lat = max(latency_dict["latency"])
    all_latency_dict = {"latency": [], "messages": []}

    # print("all_lat = ", all_lat)
    idx_lat = 1
    for lat in range(all_lat):
        all_latency_dict["latency"].append(idx_lat)
        all_latency_dict["messages"].append(0)
        idx_lat += 1

    for lat in all_latency_dict["latency"]:
        if lat in latency_dict["latency"]:
            all_latency_dict["messages"][lat - 1] = latency_dict["messages"][latency_dict["latency"].index(lat)]

    print_dict(all_latency_dict, "latency", "messages")
    check_dict(latency_dict)
    check_dict(all_latency_dict)
    return all_latency_dict

# E2C
hybrid_1_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/experiment-results/sinks/in-sink/paravance-4.rennes.grid5000.fr/opt/metrics/in-sink/latency"
hybrid_4_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/experiment-results/sinks/in-sink/paravance-56.rennes.grid5000.fr/opt/metrics/in-sink/latency"
cloud_1_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_1/20200512-180112/experiment-results/sinks/in-sink/paravance-4.rennes.grid5000.fr/opt/metrics/in-sink/latency"
cloud_4_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_4/20200513-051923/experiment-results/sinks/in-sink/paravance-71.rennes.grid5000.fr/opt/metrics/in-sink/latency"

# E2E
hybrid_1_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/experiment-results/sinks/out-sink/paravance-4.rennes.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_4_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/experiment-results/sinks/out-sink/paravance-56.rennes.grid5000.fr/opt/metrics/out-sink/latency"
cloud_1_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_1/20200512-180112/experiment-results/sinks/out-sink/paravance-4.rennes.grid5000.fr/opt/metrics/out-sink/latency"
cloud_4_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_4/20200513-051923/experiment-results/sinks/out-sink/paravance-71.rennes.grid5000.fr/opt/metrics/out-sink/latency"


hybrid_1_dict = from_csv_to_dict(hybrid_1_dstat_e2e)
cloud_1_dict = from_csv_to_dict(cloud_1_dstat_e2e)
# print(">>>>>>>>>>>>>>>>>> ", len(hybrid_4_dict["messages"]))

if len(hybrid_1_dict["messages"]) > len(cloud_1_dict["messages"]):
    for x in range((len(hybrid_1_dict["messages"])-len(cloud_1_dict["messages"]))):
        cloud_1_dict["messages"].append(0)
        cloud_1_dict["latency"].append(cloud_1_dict["latency"][len(cloud_1_dict["latency"]) - 1] + 1)
    # print(">>>>>>>>>>>>>>>>>> ", len(hybrid_4_dict["messages"]))
else:
    for x in range((len(cloud_1_dict["messages"])-len(hybrid_1_dict["messages"]))):
        hybrid_1_dict["messages"].append(0)
        hybrid_1_dict["latency"].append(hybrid_1_dict["latency"][len(hybrid_1_dict["latency"])-1]+1)


_index = hybrid_1_dict["latency"]
_data = {'hybrid_1': hybrid_1_dict["messages"], 'cloud_1': cloud_1_dict["messages"]}
dataframe = pd.DataFrame(data=_data, index=_index)

print(f">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {sum((hybrid_1_dict['latency']))} - {len((hybrid_1_dict['latency']))} - {(hybrid_1_dict['latency'])}")
print(f">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {sum((hybrid_1_dict['messages']))}m - {len((hybrid_1_dict['messages']))} - {(hybrid_1_dict['messages'])}")
print(f">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {sum((cloud_1_dict['latency']))} - {len((cloud_1_dict['latency']))} - {(cloud_1_dict['latency'])}")
print(f">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {sum((cloud_1_dict['messages']))}m - {len((cloud_1_dict['messages']))} - {(cloud_1_dict['messages'])}")


###### ABSOLUTE FREQUENCY
# dataframe.plot.hist(stacked=True)
# plt.title(_title)
# plt.xlabel(x_label, fontsize=_fontsize)
# plt.ylabel(y_label, fontsize=_fontsize)
# plt.legend()
# plt.savefig(f"{plots_path}/abs-freq_{plot_file_name}")


##### RELATIVE FREQUENCY
dataframe.drop(dataframe.index[range(7, len(dataframe))], axis=0, inplace=True)  # FIXME cut
dataframe_rel_h = dataframe['hybrid_1']/dataframe['hybrid_1'].sum()
dataframe_rel_c = dataframe['cloud_1']/dataframe['cloud_1'].sum()
dataframe['hybrid_1'] = round(dataframe_rel_h * 100)
dataframe['cloud_1'] = round(dataframe_rel_c * 100)
print("dataframe['hybrid_1'].sum() = ", dataframe['hybrid_1'].sum())
print("dataframe['cloud_1'].sum() = ", dataframe['cloud_1'].sum())
# dataframe.drop(index=4, columns='cloud_1')


### FOR BOX PLOT
def box_messages(values):
    new_values = []
    _latency = 1
    for qtd in values:
        for i in range(int(qtd)):
            new_values.append(_latency)
        _latency += 1
    print("new_values =", new_values)
    return new_values

print(dataframe)
# print(".......................................................")

# dataframe.plot.hist(stacked=True)
# BOX PLOT
_data = {'hybrid_1': box_messages(dataframe['hybrid_1'])}
dataframe_h1 = pd.DataFrame(data=_data)
# _data = {'cloud_1': box_messages(dataframe['cloud_1'])}
# dataframe_c1 = pd.DataFrame(data=_data, index=_index)
dataframe_h1.plot.box()
print(dataframe_h1)

# BAR PLOT
# dataframe.plot.bar()
# plt.xlabel(x_label, fontsize=_fontsize)
# plt.ylabel(y_label, fontsize=_fontsize)

plt.title(_title)
plt.legend()
plt.savefig(f"{plots_path}/tests/rel-freq_{plot_file_name}")


# hybrid_1_dict = from_csv_to_dict(hybrid_1_dstat_e2e)
# hybrid_4_dict = from_csv_to_dict(hybrid_4_dstat_e2e)
# # print(">>>>>>>>>>>>>>>>>> ", len(hybrid_4_dict["messages"]))
# for x in range((len(hybrid_1_dict["messages"])-len(hybrid_4_dict["messages"]))):
#     hybrid_4_dict["messages"].append(0)
# # print(">>>>>>>>>>>>>>>>>> ", len(hybrid_4_dict["messages"]))
#
# _index = hybrid_1_dict["latency"]
# # print(">>>>>>>>>>>>>>>>>> ", len(_index))
# _data = {'hybrid_1': hybrid_1_dict["messages"], 'hybrid_4': hybrid_4_dict["messages"]}
# dataframe = pd.DataFrame(data=_data, index=_index)
# dataframe.plot.bar()
# plt.title(_title)
# plt.xlabel(x_label, fontsize=_fontsize)
# plt.ylabel(y_label, fontsize=_fontsize)
# plt.legend()
# plt.savefig(f"{plots_path}/{plot_file_name}")





##### RELATIVE FREQUENCY
# dataframe.drop(dataframe.index[range(7, len(dataframe))], axis=0, inplace=True)  # FIXME cut
# dataframe_rel_h = dataframe['hybrid_1']/dataframe['hybrid_1'].sum()
# dataframe_rel_c = dataframe['cloud_1']/dataframe['cloud_1'].sum()
# dataframe['hybrid_1'] = round(dataframe_rel_h * 100)
# dataframe['cloud_1'] = round(dataframe_rel_c * 100)
# print("dataframe['hybrid_1'].sum() = ", dataframe['hybrid_1'].sum())
# print("dataframe['cloud_1'].sum() = ", dataframe['cloud_1'].sum())


