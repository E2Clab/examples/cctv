import csv
import matplotlib.pyplot as plt
import pandas as pd

MEM = 0
CPU = 4
RECV = 9
SEND = 10

def get_len(y_axes):
    lens = []

    for value in y_axes.values():
        lens.append(len(value))
    # for v in y_axes:
    #     lens.append(len(v))
    return min(lens)


def get_xy_values(csv_file, dstat_rec):
    y = []
    with open(csv_file, 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')
        iter_plots = iter(plots)
        for sk in range(6):
            next(iter_plots)
        for row in iter_plots:
            y.append(float(row[dstat_rec]))  # FIXME none float
    print("y = ", y)
    return y


# FLINK
# hybrid_4_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_4/20200512-042729/dstat/paravance-24.rennes.grid5000.fr/builds/dstat-20200512-042729-hybrid_4/dstat.csv"
# hybrid_5_dstat_flink1 = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_5/20200512-153138/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-153138-hybrid_5/dstat.csv"
# hybrid_5_dstat_flink2 = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_5/20200512-153705/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-153705-hybrid_5/dstat.csv"
# hybrid_5_dstat_flink3 = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_5/20200512-154232/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-154232-hybrid_5/dstat.csv"
# hybrid_9_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_9/20200512-155341/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-155341-hybrid_9/dstat.csv"
# hybrid_13_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_13/20200512-161011/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-161011-hybrid_13/dstat.csv"
# # KAFKA
# hybrid_1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/dstat/paravance-12.rennes.grid5000.fr/builds/dstat-20200512-152008-hybrid_1/dstat.csv"
# hybrid_5_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_5/20200512-153705/dstat/paravance-12.rennes.grid5000.fr/builds/dstat-20200512-153705-hybrid_5/dstat.csv"
# hybrid_9_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_9/20200512-155341/dstat/paravance-12.rennes.grid5000.fr/builds/dstat-20200512-155341-hybrid_9/dstat.csv"
# hybrid_13_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_13/20200512-161011/dstat/paravance-12.rennes.grid5000.fr/builds/dstat-20200512-161011-hybrid_13/dstat.csv"
# # GATEWAY
# hybrid_1_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-152008-hybrid_1/dstat.csv"
# hybrid_5_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_5/20200512-153705/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-153705-hybrid_5/dstat.csv"
# hybrid_9_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_9/20200512-155341/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-155341-hybrid_9/dstat.csv"
# hybrid_13_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_13/20200512-161011/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-161011-hybrid_13/dstat.csv"


#DESIGN
plots_path = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/plots"
plt.figure(figsize=(6, 3))
# _fontsize = 10
_linecolor_blue = "blue"
_linecolor_red = "red"
_linecolor_purple = "purple"
_linecolor_orange = "orange"
my_colors = [_linecolor_blue, _linecolor_red, _linecolor_purple, _linecolor_orange]
# plt.xticks(fontsize=_fontsize)
# plt.yticks(fontsize=_fontsize)

# DSTAT TITLES
_title_dstat_mem = 'Memory'
_title_dstat_cpu = 'CPU'
_title_dstat_net = 'Network'
x_label_dstat = 'seconds'
y_label_dstat_cpu = 'Usage (in %)'
y_label_dstat_mem = 'Gigabytes'
y_label_dstat_net = 'Bytes'

##### FLINK MEM MULTIPLOT - HYBRID
# x, y_h1 = get_xy_values(hybrid_1_dstat_flink, MEM)
# x, y_h5 = get_xy_values(hybrid_5_dstat_flink, MEM)
# x, y_h9 = get_xy_values(hybrid_9_dstat_flink, MEM)
# x, y_h13 = get_xy_values(hybrid_13_dstat_flink, MEM)
# max_len = get_len(y_h1, y_h5, y_h9, y_h13)
# hybrid_1_29_dstat_flink_mem_dataset = {'x': [], 'hybrid_1': [], 'hybrid_5': [], 'hybrid_9': [], 'hybrid_13': []}
# hybrid_1_29_dstat_flink_mem_dataset['x'] = x[:max_len]
# hybrid_1_29_dstat_flink_mem_dataset['hybrid_1'] = y_h1[:max_len]
# hybrid_1_29_dstat_flink_mem_dataset['hybrid_5'] = y_h5[:max_len]
# hybrid_1_29_dstat_flink_mem_dataset['hybrid_9'] = y_h9[:max_len]
# hybrid_1_29_dstat_flink_mem_dataset['hybrid_13'] = y_h13[:max_len]
# dataframe = pd.DataFrame(hybrid_1_29_dstat_flink_mem_dataset)
# # print(dataframe)
# plt.plot('x', 'hybrid_1', data=dataframe, color=_linecolor_blue, linewidth=1)
# plt.plot('x', 'hybrid_5', data=dataframe, color=_linecolor_red, linewidth=1)
# plt.plot('x', 'hybrid_9', data=dataframe, color=_linecolor_purple, linewidth=1)
# plt.plot('x', 'hybrid_13', data=dataframe, color=_linecolor_orange, linewidth=1)
# plt.title(_title_dstat_mem)
# plt.xlabel(x_label_stat, fontsize=_fontsize)
# plt.ylabel(y_label_stat, fontsize=_fontsize)
# plt.legend()
# plt.savefig(f"{plots_path}/flink_mem_hybrid_1_29.png")


def gen_plot(raw_data, dstat_rec, _title, x_label, y_label, plots_path, plot_file_name):
    x_axe = []
    y_axes = {}
    _dataset_dict = {}
    _labels = []

    for key, value in raw_data.items():
        _labels.append(key)
        y_axes.update({key: get_xy_values(value, dstat_rec)})

    # print("y_axes =", y_axes)
    max_len = get_len(y_axes)
    # print("max_len =", max_len)

    x_axe = list(range(1, max_len+1))
    # print("y_axes = ", y_axes)
    for key, value in y_axes.items():
        cut_arr = value[:max_len]
        y_axes.update({key: cut_arr})
    # print("y_axes = ", y_axes)

    _dataset_dict.update({'x': x_axe})
    _dataset_dict.update(y_axes)
    print(_dataset_dict)
    dataframe = pd.DataFrame(_dataset_dict)

    if dstat_rec == 0:
        dataframe.iloc[:, 1:] = dataframe.iloc[:, 1:].div(1000000000)  # GB
    if dstat_rec == 9 or dstat_rec == 10:
        dataframe.iloc[:, 1:] = dataframe.iloc[:, 1:].div(1000000)  # MB

    print("x+x+", dataframe)
    idx_color = 0

    # cloud_s1_mean_gw_recv = dataframe['cloud_s1'].median()
    # cloud_l1_mean_gw_recv = dataframe['cloud_l1'].median()
    # print("cloud_s1_mean (Mb/s) = ", cloud_s1_mean_gw_recv)
    # print("cloud_l1_mean (Mb/s) = ", cloud_l1_mean_gw_recv)

    # for key in y_axes.keys():
    #     print("key = ", key)
    #     # plt.plot('x', key, data=dataframe, color=my_colors[idx_color], linewidth=1)
    #     idx_color += 1
    _color_hybrid = 'slategray'  # [0.2, 0.4, 0.6, 0.6]
    _color_cloud = 'lightsteelblue'
    # aux_idx = 0
    if len(raw_data) == 2:
        linecolors = [_color_cloud, _color_hybrid]
        linestyles = ['-', ':']
        linewidths = [3, 3]
        # _labels = raw_data.keys()  # ['Cloud-centric processing', 'Hybrid processing']
    elif len(raw_data) == 4:
        linecolors = [_color_cloud, _color_cloud, _color_hybrid, _color_hybrid]
        linestyles = ['-', '-', ':', ':']  # ['-', '--', '-.', ':']
        linewidths = [1, 3, 1, 3]
        # _labels = raw_data.keys()  # ['Cloud 20 cameras', 'Cloud 40 cameras', 'Hybrid 20 cameras', 'Hybrid 40 cameras']

    fig, ax = plt.subplots()

    for aux_idx in range(len(raw_data)):

    # for linestly in linestyles:
        # dataframe.plot.line('x', linestyle=linestly, linewidth=2, color=linecolors[aux_idx])
        ax.plot(dataframe[_labels[aux_idx]], linestyle=linestyles[aux_idx], linewidth=linewidths[aux_idx], color=linecolors[aux_idx])
        plt.plot([], c=linecolors[aux_idx], label=_labels[aux_idx], linestyle=linestyles[aux_idx], linewidth=linewidths[aux_idx])
        # aux_idx += 1


    # plt.xlabel(x_label, fontsize=_fontsize)
    # plt.ylabel(y_label, fontsize=_fontsize)
    # ax.set_ylim([2,4])
    _fontsize = 12
    plt.rcParams.update({'font.size': _fontsize})
    plt.xticks(fontsize=_fontsize + 1)
    plt.yticks(fontsize=_fontsize + 1)
    plt.xlabel(x_label, fontsize=_fontsize)
    ax.set_ylabel(y_label, fontsize=_fontsize + 1)
    plt.title(_title, fontsize=_fontsize + 1)
    # ax.set_title(x_label, fontsize=_fontsize + 1)

    # plt.plot([], c=_color_cloud, label='Cloud 20 cameras')
    # plt.plot([], c=_color_cloud, label='Cloud 40 cameras')
    # plt.plot([], c=_color_hybrid, label='Hybrid 20 cameras')
    # plt.plot([], c=_color_hybrid, label='Hybrid 40 cameras')
    # box = plt.get_position()
    # plt.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
    # plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    plt.legend()
    plt.savefig(f"{plots_path}/tests-new/{plot_file_name}")

import numpy as np # for calculating standard deviation and mean
# import scipy.stats as sp # for calculating standard error

# def gen_plot_v2(raw_data, dstat_rec, _title, x_label, y_label, plots_path, plot_file_name):
#     x_axe = []
#     y_axes = {}
#     _dataset_dict = {}
#
#     for key, value in raw_data.items():
#         y_axes.update({key: get_xy_values(value, dstat_rec)})
#
#     # print("y_axes =", y_axes)
#     max_len = get_len(y_axes)
#     # print("max_len =", max_len)
#
#     x_axe = list(range(1, max_len+1))
#     # print("y_axes = ", y_axes)
#     for key, value in y_axes.items():
#         cut_arr = value[:max_len]
#         y_axes.update({key: cut_arr})
#     # print("y_axes = ", y_axes)
#
#     _dataset_dict.update({'x': x_axe})
#     _dataset_dict.update(y_axes)
#     print(_dataset_dict)
#     dataframe = pd.DataFrame(_dataset_dict)
#
#
#     # dataframe['hybrid_51'] = dataframe['hybrid_51'].div(1000000000)
#     dataframe.iloc[:, 1:] = dataframe.iloc[:, 1:].div(1000000000)
#
#     dataframe['avgss'] = dataframe.iloc[:, 1:4].mean(axis=1)
#     dataframe['stdss'] = dataframe.iloc[:, 1:4].std(axis=1)
#     print(">>> ", dataframe)
#     dataframe.plot(x='x', y='avgss', linestyle='-', yerr="stdss")
#
#     # idx_color = 0
#     # for key in y_axes.keys():
#     #     print("key = ", key)
#     #     plt.plot('x', key, data=dataframe, color=my_colors[idx_color], linewidth=1)
#     #     idx_color += 1
#
#     plt.title(_title)
#     plt.xlabel(x_label, fontsize=_fontsize)
#     plt.ylabel(y_label, fontsize=_fontsize)
#     plt.legend()
#     plt.savefig(f"{plots_path}/tests/{plot_file_name}")


# FLINK MEM
# raw_data_flink_mem = {}
# raw_data_flink_mem.update({'hybrid_1': hybrid_1_dstat_flink})
# raw_data_flink_mem.update({'hybrid_5': hybrid_5_dstat_flink})
# raw_data_flink_mem.update({'hybrid_9': hybrid_9_dstat_flink})
# raw_data_flink_mem.update({'hybrid_13': hybrid_13_dstat_flink})
# gen_plot(raw_data_flink_mem, MEM, f"Flink {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "flink_mem_hybrid_1_29.png")
## FLINK CPU
# raw_data_flink_cpu = {}
# raw_data_flink_cpu.update({'hybrid_1': hybrid_1_dstat_flink})
# raw_data_flink_cpu.update({'hybrid_5': hybrid_5_dstat_flink})
# raw_data_flink_cpu.update({'hybrid_9': hybrid_9_dstat_flink})
# raw_data_flink_cpu.update({'hybrid_13': hybrid_13_dstat_flink})
# gen_plot(raw_data_flink_cpu, CPU, f"Flink {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "flink_cpu_hybrid_1_29.png")
## FLINK NET - RECV
# raw_data_flink_net = {}
# raw_data_flink_net.update({'hybrid_1': hybrid_1_dstat_flink})
# raw_data_flink_net.update({'hybrid_5': hybrid_5_dstat_flink})
# raw_data_flink_net.update({'hybrid_9': hybrid_9_dstat_flink})
# raw_data_flink_net.update({'hybrid_13': hybrid_13_dstat_flink})
# gen_plot(raw_data_flink_net, RECV, f"Flink {_title_dstat_net} Usage", x_label_dstat, y_label_dstat_net, plots_path, "flink_net_hybrid_1_29.png")



## kafka MEM
# raw_data_kafka_mem = {}
# raw_data_kafka_mem.update({'hybrid_1': hybrid_1_dstat_kafka})
# raw_data_kafka_mem.update({'hybrid_5': hybrid_5_dstat_kafka})
# raw_data_kafka_mem.update({'hybrid_9': hybrid_9_dstat_kafka})
# raw_data_kafka_mem.update({'hybrid_13': hybrid_13_dstat_kafka})
# gen_plot(raw_data_kafka_mem, MEM, f"kafka {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "kafka_mem_hybrid_1_29.png")
## kafka CPU
# raw_data_kafka_cpu = {}
# raw_data_kafka_cpu.update({'hybrid_1': hybrid_1_dstat_kafka})
# raw_data_kafka_cpu.update({'hybrid_5': hybrid_5_dstat_kafka})
# raw_data_kafka_cpu.update({'hybrid_9': hybrid_9_dstat_kafka})
# raw_data_kafka_cpu.update({'hybrid_13': hybrid_13_dstat_kafka})
# gen_plot(raw_data_kafka_cpu, CPU, f"kafka {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "kafka_cpu_hybrid_1_29.png")
## kafka NET - RECV
# raw_data_kafka_net = {}
# raw_data_kafka_net.update({'hybrid_1': hybrid_1_dstat_kafka})
# raw_data_kafka_net.update({'hybrid_5': hybrid_5_dstat_kafka})
# raw_data_kafka_net.update({'hybrid_9': hybrid_9_dstat_kafka})
# raw_data_kafka_net.update({'hybrid_13': hybrid_13_dstat_kafka})
# gen_plot(raw_data_kafka_net, RECV, f"kafka {_title_dstat_net} Usage", x_label_dstat, y_label_dstat_net, plots_path, "kafka_net_hybrid_1_29.png")



# GW MEM
# raw_data_gw_mem = {}
# raw_data_gw_mem.update({'hybrid_1': hybrid_1_dstat_gw})
# raw_data_gw_mem.update({'hybrid_5': hybrid_5_dstat_gw})
# raw_data_gw_mem.update({'hybrid_9': hybrid_9_dstat_gw})
# raw_data_gw_mem.update({'hybrid_13': hybrid_13_dstat_gw})
# gen_plot(raw_data_gw_mem, MEM, f"Gateway {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "gw_mem_hybrid_1_29.png")

## gw MEM
# raw_data_gw_mem = {}
# raw_data_gw_mem.update({'hybrid_1': hybrid_1_dstat_gw})
# raw_data_gw_mem.update({'hybrid_5': hybrid_5_dstat_gw})
# raw_data_gw_mem.update({'hybrid_9': hybrid_9_dstat_gw})
# raw_data_gw_mem.update({'hybrid_13': hybrid_13_dstat_gw})
# gen_plot(raw_data_gw_mem, MEM, f"Gateway {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "gw_mem_hybrid_1_29.png")
## gw CPU
# raw_data_gw_cpu = {}
# raw_data_gw_cpu.update({'hybrid_1': hybrid_1_dstat_gw})
# raw_data_gw_cpu.update({'hybrid_5': hybrid_5_dstat_gw})
# raw_data_gw_cpu.update({'hybrid_9': hybrid_9_dstat_gw})
# raw_data_gw_cpu.update({'hybrid_13': hybrid_13_dstat_gw})
# gen_plot(raw_data_gw_cpu, CPU, f"Gateway {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "gw_cpu_hybrid_1_29.png")
# ## gw NET - RECV
# raw_data_gw_net = {}
# raw_data_gw_net.update({'hybrid_1': hybrid_1_dstat_gw})
# raw_data_gw_net.update({'hybrid_5': hybrid_5_dstat_gw})
# raw_data_gw_net.update({'hybrid_9': hybrid_9_dstat_gw})
# raw_data_gw_net.update({'hybrid_13': hybrid_13_dstat_gw})
# gen_plot(raw_data_gw_net, RECV, f"Gateway {_title_dstat_net} Usage", x_label_dstat, f"{y_label_dstat_net} recv", plots_path, "gw_recv_net_hybrid_1_29.png")
## gw NET - RECV
# raw_data_gw_net = {}
# raw_data_gw_net.update({'hybrid_1': hybrid_1_dstat_gw})
# raw_data_gw_net.update({'hybrid_5': hybrid_5_dstat_gw})
# raw_data_gw_net.update({'hybrid_9': hybrid_9_dstat_gw})
# raw_data_gw_net.update({'hybrid_13': hybrid_13_dstat_gw})
# gen_plot(raw_data_gw_net, SEND, f"Gateway {_title_dstat_net} Usage", x_label_dstat, f"{y_label_dstat_net} send", plots_path, "gw_send_net_hybrid_1_29.png")


# FLINK
# cloud_1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_1/20200512-180640/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-180640-cloud_1/dstat.csv"  #"/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_1/20200512-180112/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-180112-cloud_1/dstat.csv"
# cloud_4_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_4/20200513-052811/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200513-052811-cloud_4/dstat.csv"
# cloud_5_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_5/20200512-181739/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-181739-cloud_5/dstat.csv"
# cloud_9_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_9/20200512-183410/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-183410-cloud_9/dstat.csv"
# cloud_13_dstat_flink = ""
# # KAFKA
# cloud_1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_1/20200512-180112/dstat/paravance-12.rennes.grid5000.fr/builds/dstat-20200512-180112-cloud_1/dstat.csv"
# cloud_4_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_4/20200513-052811/dstat/paravance-14.rennes.grid5000.fr/builds/dstat-20200513-052811-cloud_4/dstat.csv"
# cloud_5_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_5/20200512-181739/dstat/paravance-12.rennes.grid5000.fr/builds/dstat-20200512-181739-cloud_5/dstat.csv"
# cloud_9_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_9/20200512-183410/dstat/paravance-12.rennes.grid5000.fr/builds/dstat-20200512-183410-cloud_9/dstat.csv"
# # cloud_13_dstat_kafka = ""
# # GATEWAY
# cloud_1_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_1/20200512-180112/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-180112-cloud_1/dstat.csv"
# cloud_4_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_4/20200513-052811/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200513-052811-cloud_4/dstat.csv"
# cloud_5_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_5/20200512-181739/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-181739-cloud_5/dstat.csv"
# cloud_9_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_9/20200512-183410/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-183410-cloud_9/dstat.csv"
# # cloud_13_dstat_gw = ""


# FLINK MEM
# raw_data_flink_mem = {}
# raw_data_flink_mem.update({'cloud_1': cloud_1_dstat_flink})
# raw_data_flink_mem.update({'cloud_5': cloud_5_dstat_flink})
# raw_data_flink_mem.update({'cloud_9': cloud_9_dstat_flink})
# # raw_data_flink_mem.update({'cloud_13': cloud_13_dstat_flink})
# gen_plot(raw_data_flink_mem, MEM, f"Flink {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "flink_mem_cloud_1_29.png")
## FLINK CPU
# raw_data_flink_cpu = {}
# raw_data_flink_cpu.update({'cloud_1': cloud_1_dstat_flink})
# raw_data_flink_cpu.update({'cloud_5': cloud_5_dstat_flink})
# raw_data_flink_cpu.update({'cloud_9': cloud_9_dstat_flink})
# # raw_data_flink_cpu.update({'cloud_13': cloud_13_dstat_flink})
# gen_plot(raw_data_flink_cpu, CPU, f"Flink {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "flink_cpu_cloud_1_29.png")
## FLINK NET - RECV
# raw_data_flink_net = {}
# raw_data_flink_net.update({'cloud_1': cloud_1_dstat_flink})
# raw_data_flink_net.update({'cloud_5': cloud_5_dstat_flink})
# raw_data_flink_net.update({'cloud_9': cloud_9_dstat_flink})
# # raw_data_flink_net.update({'cloud_13': cloud_13_dstat_flink})
# gen_plot(raw_data_flink_net, RECV, f"Flink {_title_dstat_net} Usage", x_label_dstat, y_label_dstat_net, plots_path, "flink_net_cloud_1_29.png")


## kafka MEM
# raw_data_kafka_mem = {}
# raw_data_kafka_mem.update({'cloud_1': cloud_1_dstat_kafka})
# raw_data_kafka_mem.update({'cloud_5': cloud_5_dstat_kafka})
# raw_data_kafka_mem.update({'cloud_9': cloud_9_dstat_kafka})
# # raw_data_kafka_mem.update({'cloud_13': cloud_13_dstat_kafka})
# gen_plot(raw_data_kafka_mem, MEM, f"kafka {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "kafka_mem_cloud_1_29.png")
## kafka CPU
# raw_data_kafka_cpu = {}
# raw_data_kafka_cpu.update({'cloud_1': cloud_1_dstat_kafka})
# raw_data_kafka_cpu.update({'cloud_5': cloud_5_dstat_kafka})
# raw_data_kafka_cpu.update({'cloud_9': cloud_9_dstat_kafka})
# # raw_data_kafka_cpu.update({'cloud_13': cloud_13_dstat_kafka})
# gen_plot(raw_data_kafka_cpu, CPU, f"kafka {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "kafka_cpu_cloud_1_29.png")
## kafka NET - RECV
# raw_data_kafka_net = {}
# raw_data_kafka_net.update({'cloud_1': cloud_1_dstat_kafka})
# raw_data_kafka_net.update({'cloud_5': cloud_5_dstat_kafka})
# raw_data_kafka_net.update({'cloud_9': cloud_9_dstat_kafka})
# # raw_data_kafka_net.update({'cloud_13': cloud_13_dstat_kafka})
# gen_plot(raw_data_kafka_net, RECV, f"kafka {_title_dstat_net} Usage", x_label_dstat, y_label_dstat_net, plots_path, "kafka_net_cloud_1_29.png")


## gw MEM
# raw_data_gw_mem = {}
# raw_data_gw_mem.update({'cloud_1': cloud_1_dstat_gw})
# raw_data_gw_mem.update({'cloud_5': cloud_5_dstat_gw})
# raw_data_gw_mem.update({'cloud_9': cloud_9_dstat_gw})
# # raw_data_gw_mem.update({'cloud_13': cloud_13_dstat_gw})
# gen_plot(raw_data_gw_mem, MEM, f"Gateway {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "gw_mem_cloud_1_29.png")
## gw CPU
# raw_data_gw_cpu = {}
# raw_data_gw_cpu.update({'cloud_1': cloud_1_dstat_gw})
# raw_data_gw_cpu.update({'cloud_5': cloud_5_dstat_gw})
# raw_data_gw_cpu.update({'cloud_9': cloud_9_dstat_gw})
# # raw_data_gw_cpu.update({'cloud_13': cloud_13_dstat_gw})
# gen_plot(raw_data_gw_cpu, CPU, f"Gateway {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "gw_cpu_cloud_1_29.png")
# ## gw NET - RECV
# raw_data_gw_net = {}
# raw_data_gw_net.update({'cloud_1': cloud_1_dstat_gw})
# raw_data_gw_net.update({'cloud_5': cloud_5_dstat_gw})
# raw_data_gw_net.update({'cloud_9': cloud_9_dstat_gw})
# # raw_data_gw_net.update({'cloud_13': cloud_13_dstat_gw})
# gen_plot(raw_data_gw_net, RECV, f"Gateway {_title_dstat_net} Usage", x_label_dstat, f"{y_label_dstat_net} recv", plots_path, "gw_recv_net_cloud_1_29.png")
## gw NET - RECV
# raw_data_gw_net = {}
# raw_data_gw_net.update({'cloud_1': cloud_1_dstat_gw})
# raw_data_gw_net.update({'cloud_5': cloud_5_dstat_gw})
# raw_data_gw_net.update({'cloud_9': cloud_9_dstat_gw})
# # raw_data_gw_net.update({'cloud_13': cloud_13_dstat_gw})
# gen_plot(raw_data_gw_net, SEND, f"Gateway {_title_dstat_net} Usage", x_label_dstat, f"{y_label_dstat_net} send", plots_path, "gw_send_net_cloud_1_29.png")


# FLINK
# hybrid_1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_1/20200512-151431/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-151431-hybrid_1/dstat.csv" # "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_1/20200512-152611/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-152611-hybrid_1/dstat.csv"  #"/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/dstat/paravance-10.rennes.grid5000.fr/builds/dstat-20200512-152008-hybrid_1/dstat.csv"
# hybrid_4_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/dstat/paravance-24.rennes.grid5000.fr/builds/dstat-20200512-041305-hybrid_4/dstat.csv"

# hybrid_8_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_8/20200512-051758/dstat/paravance-24.rennes.grid5000.fr/builds/dstat-20200512-051758-hybrid_8/dstat.csv"
# hybrid_12_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_12/20200512-060828/dstat/paravance-24.rennes.grid5000.fr/builds/dstat-20200512-060828-hybrid_12/dstat.csv"
# hybrid_16_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_16/20200512-070024/dstat/paravance-24.rennes.grid5000.fr/builds/dstat-20200512-070024-hybrid_16/dstat.csv"

# kafka
# hybrid_1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_1/20200512-152611/dstat/paravance-12.rennes.grid5000.fr/builds/dstat-20200512-152611-hybrid_1/dstat.csv"
# hybrid_4_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_4/20200512-043444/dstat/paravance-25.rennes.grid5000.fr/builds/dstat-20200512-043444-hybrid_4/dstat.csv" #"/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_4/20200512-030913/dstat/paravance-25.rennes.grid5000.fr/builds/dstat-20200512-030913-hybrid_4/dstat.csv"  # "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/dstat/paravance-25.rennes.grid5000.fr/builds/dstat-20200512-041305-hybrid_4/dstat.csv"
# hybrid_8_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_8/20200512-051758/dstat/paravance-25.rennes.grid5000.fr/builds/dstat-20200512-051758-hybrid_8/dstat.csv"
# hybrid_12_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_12/20200512-060828/dstat/paravance-25.rennes.grid5000.fr/builds/dstat-20200512-060828-hybrid_12/dstat.csv"
# hybrid_16_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_16/20200512-070024/dstat/paravance-25.rennes.grid5000.fr/builds/dstat-20200512-070024-hybrid_16/dstat.csv"

# gw
# hybrid_1_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_1/20200512-152611/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-152611-hybrid_1/dstat.csv"
# hybrid_4_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-041305-hybrid_4/dstat.csv"
# hybrid_8_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_8/20200512-051758/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-051758-hybrid_8/dstat.csv"
# hybrid_12_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_12/20200512-060828/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-060828-hybrid_12/dstat.csv"
# hybrid_16_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_16/20200512-070024/dstat/paravance-33.rennes.grid5000.fr/builds/dstat-20200512-070024-hybrid_16/dstat.csv"


# FLINK MEM
# raw_data_flink_mem = {}
# raw_data_flink_mem.update({'hybrid_4': hybrid_4_dstat_flink})
# raw_data_flink_mem.update({'hybrid_8': hybrid_8_dstat_flink})
# raw_data_flink_mem.update({'hybrid_12': hybrid_12_dstat_flink})
# raw_data_flink_mem.update({'hybrid_16': hybrid_16_dstat_flink})
# gen_plot(raw_data_flink_mem, MEM, f"Flink {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "flink_mem_hybrid_4_32.png")
# # FLINK CPU
# raw_data_flink_cpu = {}
# raw_data_flink_cpu.update({'hybrid_4': hybrid_4_dstat_flink})
# raw_data_flink_cpu.update({'hybrid_8': hybrid_8_dstat_flink})
# raw_data_flink_cpu.update({'hybrid_12': hybrid_12_dstat_flink})
# raw_data_flink_cpu.update({'hybrid_16': hybrid_16_dstat_flink})
# gen_plot(raw_data_flink_cpu, CPU, f"Flink {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "flink_cpu_hybrid_4_32.png")
# # FLINK NET - RECV
# raw_data_flink_net = {}
# raw_data_flink_net.update({'hybrid_4': hybrid_4_dstat_flink})
# raw_data_flink_net.update({'hybrid_8': hybrid_8_dstat_flink})
# raw_data_flink_net.update({'hybrid_12': hybrid_12_dstat_flink})
# raw_data_flink_net.update({'hybrid_16': hybrid_16_dstat_flink})
# gen_plot(raw_data_flink_net, RECV, f"Flink {_title_dstat_net} Usage", x_label_dstat, y_label_dstat_net, plots_path, "flink_net_hybrid_4_32.png")



# kafka MEM
# raw_data_kafka_mem = {}
# raw_data_kafka_mem.update({'hybrid_4': hybrid_4_dstat_kafka})
# raw_data_kafka_mem.update({'hybrid_8': hybrid_8_dstat_kafka})
# raw_data_kafka_mem.update({'hybrid_12': hybrid_12_dstat_kafka})
# raw_data_kafka_mem.update({'hybrid_16': hybrid_16_dstat_kafka})
# gen_plot(raw_data_kafka_mem, MEM, f"kafka {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "kafka_mem_hybrid_4_32.png")
# # kafka CPU
# raw_data_kafka_cpu = {}
# raw_data_kafka_cpu.update({'hybrid_4': hybrid_4_dstat_kafka})
# raw_data_kafka_cpu.update({'hybrid_8': hybrid_8_dstat_kafka})
# raw_data_kafka_cpu.update({'hybrid_12': hybrid_12_dstat_kafka})
# raw_data_kafka_cpu.update({'hybrid_16': hybrid_16_dstat_kafka})
# gen_plot(raw_data_kafka_cpu, CPU, f"kafka {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "kafka_cpu_hybrid_4_32.png")
# # kafka NET - RECV
# raw_data_kafka_net = {}
# raw_data_kafka_net.update({'hybrid_4': hybrid_4_dstat_kafka})
# raw_data_kafka_net.update({'hybrid_8': hybrid_8_dstat_kafka})
# raw_data_kafka_net.update({'hybrid_12': hybrid_12_dstat_kafka})
# raw_data_kafka_net.update({'hybrid_16': hybrid_16_dstat_kafka})
# gen_plot(raw_data_kafka_net, RECV, f"kafka {_title_dstat_net} Usage", x_label_dstat, y_label_dstat_net, plots_path, "kafka_net_hybrid_4_32.png")



# gw MEM
# raw_data_gw_mem = {}
# raw_data_gw_mem.update({'hybrid_4': hybrid_4_dstat_gw})
# raw_data_gw_mem.update({'hybrid_8': hybrid_8_dstat_gw})
# raw_data_gw_mem.update({'hybrid_12': hybrid_12_dstat_gw})
# raw_data_gw_mem.update({'hybrid_16': hybrid_16_dstat_gw})
# gen_plot(raw_data_gw_mem, MEM, f"Gateway {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "gw_mem_hybrid_4_32.png")
# gw CPU
# raw_data_gw_cpu = {}
# raw_data_gw_cpu.update({'hybrid_4': hybrid_4_dstat_gw})
# raw_data_gw_cpu.update({'hybrid_8': hybrid_8_dstat_gw})
# raw_data_gw_cpu.update({'hybrid_12': hybrid_12_dstat_gw})
# raw_data_gw_cpu.update({'hybrid_16': hybrid_16_dstat_gw})
# gen_plot(raw_data_gw_cpu, CPU, f"Gateway {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "gw_cpu_hybrid_4_32.png")
# ## gw NET - RECV
# raw_data_gw_net = {}
# raw_data_gw_net.update({'hybrid_4': hybrid_4_dstat_gw})
# raw_data_gw_net.update({'hybrid_8': hybrid_8_dstat_gw})
# raw_data_gw_net.update({'hybrid_12': hybrid_12_dstat_gw})
# raw_data_gw_net.update({'hybrid_16': hybrid_16_dstat_gw})
# gen_plot(raw_data_gw_net, RECV, f"Gateway {_title_dstat_net} Usage", x_label_dstat, f"{y_label_dstat_net} recv", plots_path, "gw_recv_net_hybrid_4_32.png")
# gw NET - RECV
# raw_data_gw_net = {}
# raw_data_gw_net.update({'hybrid_4': hybrid_4_dstat_gw})
# raw_data_gw_net.update({'hybrid_8': hybrid_8_dstat_gw})
# raw_data_gw_net.update({'hybrid_12': hybrid_12_dstat_gw})
# raw_data_gw_net.update({'hybrid_16': hybrid_16_dstat_gw})
# gen_plot(raw_data_gw_net, SEND, f"Gateway {_title_dstat_net} Usage", x_label_dstat, f"{y_label_dstat_net} send", plots_path, "gw_send_net_hybrid_4_32.png")

# H1 vs C1 MEM
# raw_data_flink_mem = {}
# raw_data_flink_mem.update({'hybrid_1': hybrid_1_dstat_flink})
# raw_data_flink_mem.update({'cloud_1': cloud_1_dstat_flink})
# gen_plot(raw_data_flink_mem, MEM, f"Flink {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "flink_mem_h1_vs_c1_29.png")

# raw_data_flink_mem.update({'hybrid_51': hybrid_5_dstat_flink1})
# raw_data_flink_mem.update({'hybrid_52': hybrid_5_dstat_flink2})
# raw_data_flink_mem.update({'hybrid_53': hybrid_5_dstat_flink3})
# gen_plot_v2(raw_data_flink_mem, MEM, f"Flink {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "flink_mem_h5_vs_c5_29.png")



# FLINK
# raw_data_flink_mem = {}
# raw_data_flink_mem.update({'hybrid_1': hybrid_1_dstat_flink})
# raw_data_flink_mem.update({'cloud_1': cloud_1_dstat_flink})
# raw_data_flink_mem.update({'hybrid_4': hybrid_4_dstat_flink})
# raw_data_flink_mem.update({'cloud_4': cloud_4_dstat_flink})
# # gen_plot(raw_data_flink_mem, MEM, f"Flink {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "flink_mem_h1_vs_c1_29.png")
# gen_plot(raw_data_flink_mem, CPU, f"Flink {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "flink_cpu_h1_vs_c1_29.png")

# KAFKA
# raw_data_kafka_mem = {}
# raw_data_kafka_mem.update({'hybrid_1': hybrid_1_dstat_kafka})
# raw_data_kafka_mem.update({'cloud_1': cloud_1_dstat_kafka})
# raw_data_kafka_mem.update({'hybrid_4': hybrid_4_dstat_kafka})
# raw_data_kafka_mem.update({'cloud_4': cloud_4_dstat_kafka})
# # gen_plot(raw_data_kafka_mem, MEM, f"Kafka {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "kafka_mem_h1_vs_c1_29.png")
# gen_plot(raw_data_kafka_mem, CPU, f"Kafka {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "kafka_cpu_h1_vs_c1_29.png")

# GW
# raw_data_gw_mem = {}
# raw_data_gw_mem.update({'hybrid_1': hybrid_1_dstat_gw})
# raw_data_gw_mem.update({'cloud_1': cloud_1_dstat_gw})
# raw_data_gw_mem.update({'hybrid_4': hybrid_4_dstat_gw})
# raw_data_gw_mem.update({'cloud_4': cloud_4_dstat_gw})
# # gen_plot(raw_data_gw_mem, MEM, f"Gateway {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "gw_mem_h1_vs_c1_29.png")
# gen_plot(raw_data_gw_mem, CPU, f"Gateway {_title_dstat_cpu} Usage", x_label_dstat, y_label_dstat_cpu, plots_path, "gw_cpu_h1_vs_c1_29.png")



############### NEWWWWWWWWWWWWWW
# cloud_s1_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_s3_20c/20200523-233352/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-233352-cloud_s3_20c/dstat.csv"  # "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_s1/20200520-170441/dstat/parasilo-12.rennes.grid5000.fr/builds/dstat-20200520-170441-cloud_s1/dstat.csv"
# cloud_s1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_s1/20200520-170441/dstat/parasilo-10.rennes.grid5000.fr/builds/dstat-20200520-170441-cloud_s1/dstat.csv"
# cloud_s1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_s1/20200520-171613/dstat/parasilo-1.rennes.grid5000.fr/builds/dstat-20200520-171613-cloud_s1/dstat.csv"
# cloud_m1_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_m1/20200521-122903/dstat/parasilo-15.rennes.grid5000.fr/builds/dstat-20200521-122903-cloud_m1/dstat.csv"
# cloud_m1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_m1/20200521-122903/dstat/parasilo-12.rennes.grid5000.fr/builds/dstat-20200521-122903-cloud_m1/dstat.csv"
# cloud_m1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_m1/20200521-123533/dstat/parasilo-10.rennes.grid5000.fr/builds/dstat-20200521-123533-cloud_m1/dstat.csv"
# cloud_l1_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_s3_40c/20200523-235832/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-235832-cloud_s3_40c/dstat.csv"  # "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l1/20200520-213155/dstat/grisou-33.nancy.grid5000.fr/builds/dstat-20200520-213155-cloud_l1/dstat.csv"
# cloud_l1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l1/20200523-170223/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-170223-cloud_l1/dstat.csv"
# cloud_l1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l1/20200523-170223/dstat/gros-46.nancy.grid5000.fr/builds/dstat-20200523-170223-cloud_l1/dstat.csv"
#
# hybrid_s1_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_s3_20c/20200523-195646/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-195646-hybrid_s3_20c/dstat.csv" #  "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_s1/20200521-175653/dstat/parasilo-12.rennes.grid5000.fr/builds/dstat-20200521-175653-hybrid_s1/dstat.csv"
# hybrid_s1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_s1/20200521-175653/dstat/parasilo-11.rennes.grid5000.fr/builds/dstat-20200521-175653-hybrid_s1/dstat.csv"
# hybrid_s1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_s1/20200521-180246/dstat/parasilo-10.rennes.grid5000.fr/builds/dstat-20200521-180246-hybrid_s1/dstat.csv"
# hybrid_m1_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_m1/20200522-000635/dstat/gros-17.nancy.grid5000.fr/builds/dstat-20200522-000635-hybrid_m1/dstat.csv"
# hybrid_m1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_m1/20200522-000037/dstat/gros-122.nancy.grid5000.fr/builds/dstat-20200522-000037-hybrid_m1/dstat.csv"
# hybrid_m1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_m1/20200522-000635/dstat/gros-102.nancy.grid5000.fr/builds/dstat-20200522-000635-hybrid_m1/dstat.csv"
# hybrid_l1_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_s3_40c/20200523-202126/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-202126-hybrid_s3_40c/dstat.csv" #  "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l1/20200521-164942/dstat/grisou-19.nancy.grid5000.fr/builds/dstat-20200521-164942-hybrid_l1/dstat.csv"
# hybrid_l1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l1/20200523-154113/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-154113-hybrid_l1/dstat.csv"
# hybrid_l1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l1/20200523-154113/dstat/gros-46.nancy.grid5000.fr/builds/dstat-20200523-154113-hybrid_l1/dstat.csv"

# FIXME 20200523-142759

# raw_data_gw_net_rec = {}
# raw_data_gw_net_rec.update({'Cloud 20 cameras': cloud_s1_dstat_gw})
# raw_data_gw_net_rec.update({'Cloud 40 cameras': cloud_l1_dstat_gw})
# raw_data_gw_net_rec.update({'Hybrid 20 cameras': hybrid_s1_dstat_gw})
# raw_data_gw_net_rec.update({'Hybrid 40 cameras': hybrid_l1_dstat_gw})
# # gen_plot(raw_data_gw_net_rec, MEM, f"Gateway {_title_dstat_mem} Usage", x_label_dstat, 'GB', plots_path, "gw_mem_20_40_1gbit-1gbit.png")
# gen_plot(raw_data_gw_net_rec, CPU, f"Gateway {_title_dstat_cpu} Usage", x_label_dstat, 'usage in %', plots_path, "gw_cpu_20_40_1gbit-1gbit.png")
# # gen_plot(raw_data_gw_net_rec, RECV, f"Gateway {_title_dstat_net} Usage", x_label_dstat, 'MB/s', plots_path, "gw_net_rec_s1_l1_c_vs_h.png")


# raw_data_gw_net_rec = {}
# cloud_s0_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_s0_20c/20200523-212239/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-212239-cloud_s0_20c/dstat.csv"
# cloud_l0_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_s0_40c/20200523-214727/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-214727-cloud_s0_40c/dstat.csv"
# hybrid_s0_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_s0_20c/20200523-191931/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-191931-hybrid_s0_20c/dstat.csv"
# hybrid_l0_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_s0_40c/20200523-194427/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-194427-hybrid_s0_40c/dstat.csv"
# raw_data_gw_net_rec.update({'Cloud 20 cameras': cloud_s0_dstat_gw})
# raw_data_gw_net_rec.update({'Cloud 40 cameras': cloud_l0_dstat_gw})
# raw_data_gw_net_rec.update({'Hybrid 20 cameras': hybrid_s0_dstat_gw})
# raw_data_gw_net_rec.update({'Hybrid 40 cameras': hybrid_l0_dstat_gw})
# gen_plot(raw_data_gw_net_rec, MEM, f"Gateway {_title_dstat_mem} Usage", x_label_dstat, 'GB', plots_path, "gw_mem_20_40_10gbit-10gbit.png")
# # gen_plot(raw_data_gw_net_rec, CPU, f"Gateway {_title_dstat_cpu} Usage", x_label_dstat, 'usage in %', plots_path, "gw_cpu_20_40_10gbit-10gbit.png")
# ### gen_plot(raw_data_gw_net_rec, RECV, f"Gateway {_title_dstat_net} Usage", x_label_dstat, 'MB/s', plots_path, "gw_net_rec_s1_l1_c_vs_h.png")


raw_data_gw_net_rec = {}
cloud_s0_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_s3_20c/20200523-233352/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-233352-cloud_s3_20c/dstat.csv"
# cloud_l0_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_s3_40c/20200523-235205/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-235205-cloud_s3_40c/dstat.csv"
cloud_l0_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_s3_40c/20200523-235832/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-235832-cloud_s3_40c/dstat.csv"
hybrid_s0_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_s3_20c/20200523-195646/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-195646-hybrid_s3_20c/dstat.csv"
hybrid_l0_dstat_gw = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_s3_40c/20200523-202126/dstat/gros-57.nancy.grid5000.fr/builds/dstat-20200523-202126-hybrid_s3_40c/dstat.csv"
raw_data_gw_net_rec.update({'Cloud 20 cameras': cloud_s0_dstat_gw})
raw_data_gw_net_rec.update({'Cloud 40 cameras': cloud_l0_dstat_gw})
raw_data_gw_net_rec.update({'Hybrid 20 cameras': hybrid_s0_dstat_gw})
raw_data_gw_net_rec.update({'Hybrid 40 cameras': hybrid_l0_dstat_gw})
gen_plot(raw_data_gw_net_rec, MEM, f"Gateway {_title_dstat_mem} Usage", x_label_dstat, 'GB', plots_path, "gw_mem_20_40_1gbit-1gbit.png")
# gen_plot(raw_data_gw_net_rec, CPU, f"Gateway {_title_dstat_cpu} Usage", x_label_dstat, 'usage in %', plots_path, "gw_cpu_20_40_1gbit-1gbit.png")
### gen_plot(raw_data_gw_net_rec, RECV, f"Gateway {_title_dstat_net} Usage



# raw_data_kafka_net_rec = {}
# # raw_data_kafka_net_rec.update({'cloud_s1': cloud_s1_dstat_kafka})
# raw_data_kafka_net_rec.update({'Cloud-centric processing': cloud_l1_dstat_kafka})
# # raw_data_kafka_net_rec.update({'hybrid_s1': hybrid_s1_dstat_kafka})
# raw_data_kafka_net_rec.update({'Hybrid processing': hybrid_l1_dstat_kafka})
# # gen_plot(raw_data_kafka_net_rec, MEM, f"Kafka {_title_dstat_mem} Usage", x_label_dstat, 'GB', plots_path, "kafka_mem_cost_150mbit-1gbit.png")
# # gen_plot(raw_data_kafka_net_rec, CPU, f"Kafka {_title_dstat_cpu} Usage", x_label_dstat, 'usage in %', plots_path, "kafka_cpu_rec_s1_l1_c_vs_h.png")
# gen_plot(raw_data_kafka_net_rec, RECV, f"Kafka {_title_dstat_net} Usage", x_label_dstat, 'Recv MB/s', plots_path, "kafka_net_cost_150mbit-1gbit.png")


raw_data_kafka_net_rec = {}

# cloud_l0_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l0/20200526-034648/dstat/paravance-28.rennes.grid5000.fr/builds/dstat-20200526-034648-cloud_l0/dstat.csv"
cloud_l0_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l0/20200524-184100/dstat/paravance-28.rennes.grid5000.fr/builds/dstat-20200524-184100-cloud_l0/dstat.csv"
# cloud_l0_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l0/20200526-040305/dstat/paravance-28.rennes.grid5000.fr/builds/dstat-20200526-040305-cloud_l0/dstat.csv"
# hybrid_l0_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l0/20200526-020635/dstat/paravance-28.rennes.grid5000.fr/builds/dstat-20200526-020635-hybrid_l0/dstat.csv"
hybrid_l0_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l0/20200524-181848/dstat/paravance-28.rennes.grid5000.fr/builds/dstat-20200524-181848-hybrid_l0/dstat.csv"
# hybrid_l0_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l0/20200526-033910/dstat/paravance-28.rennes.grid5000.fr/builds/dstat-20200526-033910-hybrid_l0/dstat.csv"

cloud_l0_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l0/20200524-184100/dstat/grisou-22.nancy.grid5000.fr/builds/dstat-20200524-184100-cloud_l0/dstat.csv"
hybrid_l0_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l0/20200524-181848/dstat/grisou-22.nancy.grid5000.fr/builds/dstat-20200524-181848-hybrid_l0/dstat.csv"

# cloud_l1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l1/20200523-170223/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-170223-cloud_l1/dstat.csv"
# hybrid_l1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l1/20200523-142759/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-142759-hybrid_l1/dstat.csv"

# cloud_l3p_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l3p/20200523-174942/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-174942-cloud_l3p/dstat.csv"
# cloud_l3p_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l3p/20200523-175623/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-175623-cloud_l3p/dstat.csv"
# hybrid_l3p_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l3p/20200523-162842/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-162842-hybrid_l3p/dstat.csv"
# hybrid_l3p_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l3p/20200523-163525/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-163525-hybrid_l3p/dstat.csv"


# cloud_l1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l1/20200523-170223/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-170223-cloud_l1/dstat.csv"
# cloud_l3p_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l3p/20200523-175623/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-175623-cloud_l3p/dstat.csv"
# hybrid_l1_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l1/20200523-154113/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-154113-hybrid_l1/dstat.csv"
# hybrid_l3p_dstat_kafka = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l3p/20200523-163525/dstat/gros-49.nancy.grid5000.fr/builds/dstat-20200523-163525-hybrid_l3p/dstat.csv"


# raw_data_kafka_net_rec.update({'cloud_s1': cloud_s1_dstat_kafka})
raw_data_kafka_net_rec.update({'Cloud-centric processing': cloud_l0_dstat_kafka})
# raw_data_kafka_net_rec.update({'hybrid_s1': hybrid_s1_dstat_kafka})
raw_data_kafka_net_rec.update({'Hybrid processing': hybrid_l0_dstat_kafka})
gen_plot(raw_data_kafka_net_rec, MEM, f"Kafka {_title_dstat_mem} Usage (Large Scenario)", x_label_dstat, 'GB', plots_path, "kafka_mem_cost_10gbit-10gbit.png")
# gen_plot(raw_data_kafka_net_rec, CPU, f"Kafka {_title_dstat_cpu} Usage", x_label_dstat, 'usage in %', plots_path, "kafka_cpu_cost_10gbit-10gbit.png")
# gen_plot(raw_data_kafka_net_rec, SEND, f"Kafka {_title_dstat_net} Usage", x_label_dstat, 'Send MB/s', plots_path, "kafka_net_send_cost_10gbit-10gbit.png")





# raw_data_flink_net_rec = {}
# # raw_data_flink_net_rec.update({'cloud_s1': cloud_s1_dstat_flink})
# raw_data_flink_net_rec.update({'Cloud-centric processing': cloud_l1_dstat_flink})
# # raw_data_flink_net_rec.update({'hybrid_s1': hybrid_s1_dstat_flink})
# raw_data_flink_net_rec.update({'Hybrid processing': hybrid_l1_dstat_flink})
# gen_plot(raw_data_flink_net_rec, MEM, f"Flink {_title_dstat_mem} Usage", x_label_dstat, 'GB', plots_path, "flink_mem_rec_s1_l1_c_vs_h.png")
# # gen_plot(raw_data_flink_net_rec, CPU, f"Flink {_title_dstat_cpu} Usage", x_label_dstat, 'usage in %', plots_path, "flink_cpu_rec_s1_l1_c_vs_h.png")
# # gen_plot(raw_data_flink_net_rec, RECV, f"Flink {_title_dstat_net} Usage", x_label_dstat, 'Send MB/s', plots_path, "flink_net_rec_s1_l1_c_vs_h.png")


raw_data_flink_net_rec = {}
cloud_l1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l0/20200524-184100/dstat/grisou-21.nancy.grid5000.fr/builds/dstat-20200524-184100-cloud_l0/dstat.csv"
hybrid_l1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l0/20200524-181848/dstat/grisou-21.nancy.grid5000.fr/builds/dstat-20200524-181848-hybrid_l0/dstat.csv"
# # cloud_l1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/cloud_l3p/20200523-175623/dstat/gros-46.nancy.grid5000.fr/builds/dstat-20200523-175623-cloud_l3p/dstat.csv"
# # hybrid_l1_dstat_flink = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/hybrid_l3p/20200523-163525/dstat/gros-46.nancy.grid5000.fr/builds/dstat-20200523-163525-hybrid_l3p/dstat.csv"
# # raw_data_flink_net_rec.update({'cloud_s1': cloud_s1_dstat_flink})
raw_data_flink_net_rec.update({'Cloud-centric processing': cloud_l1_dstat_flink})
# # raw_data_flink_net_rec.update({'hybrid_s1': hybrid_s1_dstat_flink})
raw_data_flink_net_rec.update({'Hybrid processing': hybrid_l1_dstat_flink})
gen_plot(raw_data_flink_net_rec, MEM, f"Flink {_title_dstat_mem} Usage (Large Scenario)", x_label_dstat, 'GB', plots_path, "flink_mem_cost_10gbit-10gbit.png")
# # gen_plot(raw_data_flink_net_rec, CPU, f"Flink {_title_dstat_cpu} Usage", x_label_dstat, 'usage in %', plots_path, "flink_cpu_rec_s1_l1_c_vs_h.png")
# # gen_plot(raw_data_flink_net_rec, RECV, f"Flink {_title_dstat_net} Usage", x_label_dstat, 'Send MB/s', plots_path, "flink_net_rec_s1_l1_c_vs_h.png")