import csv
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plots_path = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/plots"
plot_file_name = "end-to-end-latency.png"


def all_messages(values):
    new_values = []
    _latency = 1
    for qtd in values:
        for i in range(int(qtd)):
            new_values.append(_latency)
        _latency += 1
    print("new_values =", new_values)
    return new_values


def print_dict(id, _dict, h1, h2):
    print(f"{id} = {_dict}")
    print(f"{id} len[{h1}] = {len(_dict[h1])}")
    print(f"{id} len[{h2}] = {len(_dict[h2])}")


def check_dict(id, _dict):
    sum_l = 0
    sum_m = 0
    for lts in _dict["latency"]:
        sum_l += lts
    for msg in _dict["messages"]:
        sum_m += msg
    print(f"[check_dict]{id} - len[latency]={len(_dict['latency'])}, max[latency]={max(_dict['latency'])}, sum_m={sum_m}")


def from_csv_to_dict(csv_file_path, scn_id):
    f = open(csv_file_path, "r")
    file_data = f.read()
    csv_values = iter(file_data.split(' '))
    # next(csv_values)
    next(csv_values)
    latency_values = []
    message_values = []
    # idx_values = 1
    for _data in csv_values:
        # if idx_values > 2:
        #     break
        d = _data.split(',')
        latency_values.append(int(d[0]))
        message_values.append(int(d[1]))
        # idx_values += 1

    latency_dict = {"latency": latency_values, "messages": message_values}
    print_dict('source', latency_dict, "latency", "messages")

    all_lat = max(latency_dict["latency"])
    all_latency_dict = {"id": scn_id, "latency": [], "messages": []}

    # print("all_lat = ", all_lat)
    idx_lat = 1
    for lat in range(all_lat):
        all_latency_dict["latency"].append(idx_lat)
        all_latency_dict["messages"].append(0)
        idx_lat += 1

    for lat in all_latency_dict["latency"]:
        if lat in latency_dict["latency"]:
            all_latency_dict["messages"][lat - 1] = latency_dict["messages"][latency_dict["latency"].index(lat)]

    print_dict('zeros', all_latency_dict, "latency", "messages")
    check_dict('source', latency_dict)
    check_dict('zeros', all_latency_dict)
    return all_latency_dict


def calc_relative_freq_of_msgs(_dict):
    key = "messages"
    _dataframe = pd.DataFrame(data=_dict)
    dataframe_rel = _dataframe[key] / _dataframe[key].sum()
    _dataframe[key] = round(dataframe_rel * 100)
    print(f">>> dataframe[{key}].sum() = {int(_dataframe[key].sum())}")
    return _dataframe, int(_dataframe[key].sum())


def add_scenario_to_dataframe(scenarios, df):
    min_len = 100
    _rel_freqs = []

    for scenario in scenarios:
        _rel_freq, _size = calc_relative_freq_of_msgs(scenario)
        _rel_freqs.append({'id': scenario['id'], 'rf': _rel_freq})
        min_len = _size if min_len > _size else min_len
    print("selected min_len = ", min_len)

    for _rel_freq in _rel_freqs:
        # print(_rel_freq)  # print dataframe
        all_msgs = all_messages(_rel_freq['rf']['messages'])
        print(f">>> len of all_msgs = {len(all_msgs)}")
        df.insert(0, _rel_freq['id'], all_msgs[:min_len], True)


def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

# set_box_color(bpl, '#D7191C') # colors are from http://colorbrewer2.org/

# ########################################## NEW
# Figure 2: 22 Mb 1 Gb / 3 Workloads
# 22Mb, 1Gb
# SMALL
# hybrid_s2_e2e_s = "/home/drosendo/Documents/plots-pd/hybrid_s2_22mbit_1gbit"
# cloud_s2_e2e_s = "/home/drosendo/Documents/plots-pd/cloud_s2_22mbit_1gbit"
# hybrid_s2_e2e_s_dict = from_csv_to_dict(hybrid_s2_e2e_s, 'hybrid_s2')
# cloud_s2_e2e_s_dict = from_csv_to_dict(cloud_s2_e2e_s, 'cloud_s2')
# # MEDIUM
# hybrid_m2_e2e_m = "/home/drosendo/Documents/plots-pd/hybrid_s2_10gbit_10gbit"
cloud_m2_e2e_m = "/home/drosendo/Documents/plots-pd/cloud_s2_10gbit_10gbit"
# hybrid_m2_e2e_m_dict = from_csv_to_dict(hybrid_m2_e2e_m, 'hybrid_m2')
cloud_m2_e2e_m_dict = from_csv_to_dict(cloud_m2_e2e_m, 'cloud_m2')
# LARGE
hybrid_l2_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l2/20200523-155454/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"  # "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_l2/20200523-154810/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
cloud_l2_e2e_l = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l2/20200523-171549/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency" # "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/cloud_l2/20200523-170909/experiment-results/sinks/out-sink/gros-80.nancy.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_l2_e2e_l_dict = from_csv_to_dict(hybrid_l2_e2e_l, 'hybrid_l2')
cloud_l2_e2e_l_dict = from_csv_to_dict(cloud_l2_e2e_l, 'cloud_l2')


scenarios = [hybrid_s2_e2e_s_dict, cloud_s2_e2e_s_dict,
             hybrid_m2_e2e_m_dict, cloud_m2_e2e_m_dict]#,
             # hybrid_l2_e2e_l_dict, cloud_l2_e2e_l_dict]
df = pd.DataFrame(data={})
add_scenario_to_dataframe(scenarios, df)
print(df)

data1 = [df['hybrid_s2'], df['cloud_s2']]
data2 = [df['hybrid_m2'], df['cloud_m2']]
# data3 = [df['hybrid_l2'], df['cloud_l2']]

fig, ax = plt.subplots()

# labels1 = ['22mbit, 1gbit', '22mbit, 1gbit']
# labels2 = ['1gbit, 1gbit', '1gbit, 1gbit']
# labels3 = ['1gbit, 22mbit', '1gbit, 22mbit']
new_lbl = ['Small', 'Medium', 'Large']

medianprops = dict(linestyle='-', linewidth=1.5, color='black')
bplot1 = ax.boxplot(data1,

                    positions=[-0.4, 0.4], sym='',
                    # positions=[1,2],
                    widths=0.4,
                    vert=True,  # vertical box alignment
                    patch_artist=True,  # fill with color
                    labels=None,
                    medianprops=medianprops)  # will be used to label x-ticks

bplot2 = ax.boxplot(data2,
                    positions=[1.6, 2.4], sym='',
                    # positions=[3,4],
                    widths=0.4, vert=True, patch_artist=True, labels=None, medianprops=medianprops)

# bplot3 = ax.boxplot(data3,
#                     # positions=np.array(range(len(data3)))*2.0-0.4, sym='',
#                     positions=[3.6, 4.4],
#                     widths=0.4, vert=True, patch_artist=True, labels=None, medianprops=medianprops)

# fill with colors

_color_a = 'slategray'
_color_b = 'lightsteelblue'
colors = [_color_a, _color_b]
for bplot in (bplot1, bplot2):
    for patch, color in zip(bplot['boxes'], colors):
        patch.set_facecolor(color)

# draw temporary red and blue lines and use them to create a legend
# plt.plot([], c='#D7191C', label='Apples')
# plt.plot([], c='#2C7BB6', label='Oranges')
# plt.legend()
# Set the axes ranges and axes labels

# num_boxes = 6
# ax.set_xlim(0.5, num_boxes + 0.5)
# ax.set_xticklabels(labels1 + labels2 + labels3, rotation=15, fontsize=10)
# ax.set_xticklabels(new_lbl, rotation=15, fontsize=8)


plt.xticks(range(0, len(new_lbl) * 2, 2), new_lbl)
# plt.xlim(-2, len(new_lbl)*2)


ax.set_title('End-to-end Latency (22mbit-1gbit)')
ax.legend([bplot1["boxes"][0], bplot1["boxes"][1]], ['Hybrid', 'Cloud'], loc="upper left", bbox_transform=fig.transFigure)
# plt.tick_params(axis='x', pad=3)
fig.tight_layout()
plt.savefig(f"{plots_path}/tests-new/e2e-l-figure-2-SML-1-Network-22-1.png")
plt.show()


# print(np.array(range(len(data3)))*2.0-0.4)
# print(np.array(range(len(data3)))*2.0+0.4)