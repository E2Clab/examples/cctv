import csv
import matplotlib.pyplot as plt
import pandas as pd

MEM = 0
CPU = 4
RECV = 9
SEND = 10

def get_len(y_axes):
    lens = []

    for value in y_axes.values():
        lens.append(len(value))
    # for v in y_axes:
    #     lens.append(len(v))
    return min(lens)


def get_xy_values(csv_file, dstat_rec):
    y = []
    with open(csv_file, 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')
        iter_plots = iter(plots)
        for sk in range(6):
            next(iter_plots)
        for row in iter_plots:
            y.append(float(row[dstat_rec]))  # FIXME
    # print("y = ", y)
    return y


#DESIGN
plots_path = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/plots"
plt.figure(figsize=(6, 3))
_fontsize = 10
_linecolor_blue = "blue"
_linecolor_red = "red"
_linecolor_purple = "purple"
_linecolor_orange = "orange"
my_colors = [_linecolor_blue, _linecolor_red, _linecolor_purple, _linecolor_orange]
plt.xticks(fontsize=_fontsize)
plt.yticks(fontsize=_fontsize)

# DSTAT TITLES
_title_dstat_mem = 'Memory'
_title_dstat_cpu = 'CPU'
_title_dstat_net = 'Network'
x_label_dstat = 'seconds'
y_label_dstat_cpu = 'usage (in %)'
y_label_dstat_mem = 'bytes'
y_label_dstat_net = 'bytes'


def gen_plot_dstat(raw_data, dstat_rec, _title, x_label, y_label, plots_path, plot_file_name):
    x_axe = []
    y_axes = {}
    _dataset_dict = {}

    for key, value in raw_data.items():
        y_axes.update({key: get_xy_values(value, dstat_rec)})

    # print("y_axes =", y_axes)
    max_len = get_len(y_axes)
    # print("max_len =", max_len)

    x_axe = list(range(1, max_len+1))
    # print("y_axes = ", y_axes)
    for key, value in y_axes.items():
        cut_arr = value[:max_len]
        y_axes.update({key: cut_arr})
    # print("y_axes = ", y_axes)

    _dataset_dict.update({'x': x_axe})
    _dataset_dict.update(y_axes)
    # print(_dataset_dict)
    dataframe = pd.DataFrame(_dataset_dict)
    print(dataframe)
    idx_color = 0
    for key in y_axes.keys():
        # print("key = ", key)
        plt.plot('x', key, data=dataframe, color=my_colors[idx_color], linewidth=1)
        idx_color += 1

    plt.title(_title)
    plt.xlabel(x_label, fontsize=_fontsize)
    plt.ylabel(y_label, fontsize=_fontsize)

    # box = plt.get_position()
    # plt.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
    # plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    plt.legend()
    plt.savefig(f"{plots_path}/{plot_file_name}")


def gen_plot_dstat(raw_data, dstat_rec, _title, x_label, y_label, plots_path, plot_file_name):
    x_axe = []
    y_axes = {}
    _dataset_dict = {}

    for key, value in raw_data.items():
        y_axes.update({key: get_xy_values(value, dstat_rec)})

    # print("y_axes =", y_axes)
    max_len = get_len(y_axes)
    # print("max_len =", max_len)

    x_axe = list(range(1, max_len+1))
    # print("y_axes = ", y_axes)
    for key, value in y_axes.items():
        cut_arr = value[:max_len]
        y_axes.update({key: cut_arr})
    # print("y_axes = ", y_axes)

    _dataset_dict.update({'x': x_axe})
    _dataset_dict.update(y_axes)
    # print(_dataset_dict)
    dataframe = pd.DataFrame(_dataset_dict)
    print(dataframe)
    idx_color = 0
    for key in y_axes.keys():
        # print("key = ", key)
        plt.plot('x', key, data=dataframe, color=my_colors[idx_color], linewidth=1)
        idx_color += 1

    plt.title(_title)
    plt.xlabel(x_label, fontsize=_fontsize)
    plt.ylabel(y_label, fontsize=_fontsize)

    # box = plt.get_position()
    # plt.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
    # plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)
    plt.legend()
    plt.savefig(f"{plots_path}/{plot_file_name}")


# E2C
hybrid_1_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/experiment-results/sinks/in-sink/paravance-4.rennes.grid5000.fr/opt/metrics/in-sink/latency"
hybrid_4_dstat_e2c = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/experiment-results/sinks/in-sink/paravance-56.rennes.grid5000.fr/opt/metrics/in-sink/latency"

# E2E
hybrid_1_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_1/20200512-152008/experiment-results/sinks/out-sink/paravance-4.rennes.grid5000.fr/opt/metrics/out-sink/latency"
hybrid_4_dstat_e2e = "/home/drosendo/git/e2clab-examples/cctv/experiments/scenarios/hybrid_4/20200512-041305/experiment-results/sinks/out-sink/paravance-56.rennes.grid5000.fr/opt/metrics/out-sink/latency"



# raw_data_kafka_mem = {}
# raw_data_kafka_mem.update({'hybrid_4': hybrid_4_dstat_kafka})
# raw_data_kafka_mem.update({'hybrid_8': hybrid_8_dstat_kafka})
# raw_data_kafka_mem.update({'hybrid_12': hybrid_12_dstat_kafka})
# raw_data_kafka_mem.update({'hybrid_16': hybrid_16_dstat_kafka})
# gen_plot(raw_data_kafka_mem, MEM, f"kafka {_title_dstat_mem} Usage", x_label_dstat, y_label_dstat_mem, plots_path, "kafka_mem_hybrid_4_32.png")


