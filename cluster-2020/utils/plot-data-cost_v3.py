import csv
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


plots_path = "/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios/plots"
# TO_MB = 1000000
# TIMEOUT = 220
# all_prod = []
# all_recv = []
# all_send = []
site = 'nancy'
cluster = 'gros'
NUM_PRODUCERS = 40


def autolabel(rects, xpos='center'):
    """
    Attach a text label above each bar in *rects*, displaying its height.

    *xpos* indicates which side to place the text w.r.t. the center of
    the bar. It can be one of the following {'center', 'right', 'left'}.
    """

    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0, 'right': 1, 'left': -1}

    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(offset[xpos]*3, 3),  # use 3 points offset
                    textcoords="offset points",  # in both directions
                    ha=ha[xpos], va='bottom')


def from_csv_to_arr(csv_file_path):
    data = []
    for csv_file in csv_file_path:
        # print(csv_file)
        f = open(csv_file, 'r')
        n_messages = 0
        time_in_ms = 0
        l_idx = 0
        for l in f.readlines():
            if l_idx == 0:
                n_messages = int(l.split(':')[1])
            elif l_idx == 1:
                time_in_ms = int(l.split(':')[1])
            l_idx += 1

        if n_messages == 0 or time_in_ms == 0:
            print(f"-------------------- excluding = {csv_file}")
        else:
            # msgs_s = (n_messages / (time_in_ms / 1000))  # FIXME messages/second
            msgs_s = n_messages * (0.025 / 1000)   # FIXME amount of data (in GB), 1 image = 0.025 MB
            data.append(msgs_s)
        # if '_l1' in csv_file:
        #     if 'producers' in csv_file:
        #         print(f"th[{msgs_s}] = {csv_file}")
        # print(n_messages)
        # print(time_in_ms)
        # print(round(msgs_s))
    # print(data)
    remove_outliers(data)
    return data


def remove_outliers(_data):
    _mean = np.array(_data).mean()
    for _d in _data:
        if _d > 4*_mean:
            print(f"removing outlier = {_d}, mean[{_mean}]")
            _data.remove(_d)


def gen_path_to_files_gw(ids, name, exp_id, cluster, site):
    _prefix = '/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios'
    paths = []
    for id in ids:
        paths.append(f"{_prefix}/{name}/{exp_id}/experiment-results/gateways/{cluster}-{id}.{site}.grid5000.fr/opt/metrics/throughput")
    return paths


def gen_path_to_files_pd(ids, name, exp_id, cluster, site):
    _prefix = '/home/drosendo/git/c2tbench-examples/cctv/experiments/scenarios'
    paths = []
    for _id in ids:
        id_cam = 1
        for cam in range(NUM_PRODUCERS):
            paths.append(f"{_prefix}/{name}/{exp_id}/experiment-results/producers/{cluster}-{_id}.{site}.grid5000.fr/opt/metrics-{id_cam}/throughput")
            id_cam += 1
    return paths


def format_value(values):
    new_val = []
    for val in values:
        new_val.append(float("{:.2f}".format(val)))
    return new_val


def check_all_nodes(scenario, producers_cloud, gateways_cloud, producers_hybrid, gateways_hybrid):
    print(f"Scenario [{scenario}], "
          f"producers_cloud [{len(producers_cloud)}], gateways_cloud [{len(gateways_cloud)}], "
          f"producers_hybrid [{len(producers_hybrid)}], gateways_hybrid [{len(gateways_hybrid)}].")


def multiply_arr(item_s, item_m, item_l, by_s, by_m, by_l):
    return np.multiply([item_s, item_m, item_l], [by_s, by_m, by_l])


# print("all_prod = ", all_prod)
# print("all_recv = ", all_recv)
# print("all_send = ", all_send)

# CSV FILES
# SMALL
cloud_producers_id_s = [52]
cloud_gateways_id_s = [51]
hybrid_producers_id_s = [52]
hybrid_gateways_id_s = [51]
producers_th_cloud_s1_csv_files = gen_path_to_files_pd(cloud_producers_id_s, 'cloud_s0', '20200524-213729', cluster, site)
gateways_th_cloud_s1_csv_files = gen_path_to_files_gw(cloud_gateways_id_s, 'cloud_s0', '20200524-213729', cluster, site)
producers_th_hybrid_s1_csv_files = gen_path_to_files_pd(hybrid_producers_id_s, 'hybrid_s0', '20200524-212117', cluster, site)
gateways_th_hybrid_s1_csv_files = gen_path_to_files_gw(hybrid_gateways_id_s, 'hybrid_s0', '20200524-212117', cluster, site)
# MEDIUM
cloud_producers_id_m = [5, 49, 50, 51]
cloud_gateways_id_m = [109, 110, 113, 114]  # FIXME add all gateways
hybrid_producers_id_m = [5, 49, 50, 51]
hybrid_gateways_id_m = [109, 110, 113, 114]  # FIXME add all gateways
producers_th_cloud_m1_csv_files = gen_path_to_files_pd(cloud_producers_id_m, 'cloud_m0', '20200524-195038', cluster, site)
gateways_th_cloud_m1_csv_files = gen_path_to_files_gw(cloud_gateways_id_m, 'cloud_m0', '20200524-195038', cluster, site)
producers_th_hybrid_m1_csv_files = gen_path_to_files_pd(hybrid_producers_id_m, 'hybrid_m0', '20200524-193407', cluster, site)
gateways_th_hybrid_m1_csv_files = gen_path_to_files_gw(hybrid_gateways_id_m, 'hybrid_m0', '20200524-193407', cluster, site)
# LARGE
cloud_producers_id_l = [38, 47, 49, 5, 50, 51, 6, 7, 39, 4, 40, 41, 43, 44, 45, 46]
cloud_gateways_id_l = [23, 31, 32, 33, 34, 35, 36, 37, 24, 25, 26, 27, 28, 29, 3, 30]  # FIXME add all gateways
hybrid_producers_id_l = [38, 47, 49, 5, 50, 51, 6, 7, 39, 4, 40, 41, 43, 44, 45, 46]
hybrid_gateways_id_l = [23, 31, 32, 33, 34, 35, 36, 37, 24, 25, 26, 27, 28, 29, 3, 30]  # FIXME add all gateways
producers_th_cloud_l1_csv_files = gen_path_to_files_pd(cloud_producers_id_l, 'cloud_l0', '20200524-184100', 'grisou', site)
gateways_th_cloud_l1_csv_files = gen_path_to_files_gw(cloud_gateways_id_l, 'cloud_l0', '20200524-184100', 'grisou', site)
producers_th_hybrid_l1_csv_files = gen_path_to_files_pd(hybrid_producers_id_l, 'hybrid_l0', '20200524-181848', 'grisou', site)
gateways_th_hybrid_l1_csv_files = gen_path_to_files_gw(hybrid_gateways_id_l, 'hybrid_l0', '20200524-181848', 'grisou', site)


# RAW DATA
# SMALL
producers_th_cloud_s1_arr = np.array(from_csv_to_arr(producers_th_cloud_s1_csv_files))  # * NUM_PRODUCERS
gateways_th_cloud_s1_arr = np.array(from_csv_to_arr(gateways_th_cloud_s1_csv_files))
producers_th_hybrid_s1_arr = np.array(from_csv_to_arr(producers_th_hybrid_s1_csv_files))  # * NUM_PRODUCERS
gateways_th_hybrid_s1_arr = np.array(from_csv_to_arr(gateways_th_hybrid_s1_csv_files))
check_all_nodes('SMALL', producers_th_cloud_s1_arr, gateways_th_cloud_s1_arr, producers_th_hybrid_s1_arr, gateways_th_hybrid_s1_arr)
# MEDIUM
producers_th_cloud_m1_arr = np.array(from_csv_to_arr(producers_th_cloud_m1_csv_files))  # * NUM_PRODUCERS
gateways_th_cloud_m1_arr = np.array(from_csv_to_arr(gateways_th_cloud_m1_csv_files))
producers_th_hybrid_m1_arr = np.array(from_csv_to_arr(producers_th_hybrid_m1_csv_files))  # * NUM_PRODUCERS
gateways_th_hybrid_m1_arr = np.array(from_csv_to_arr(gateways_th_hybrid_m1_csv_files))
check_all_nodes('MEDIUM', producers_th_cloud_m1_arr, gateways_th_cloud_m1_arr, producers_th_hybrid_m1_arr, gateways_th_hybrid_m1_arr)
# LARGE
producers_th_cloud_l1_arr = np.array(from_csv_to_arr(producers_th_cloud_l1_csv_files))  # * NUM_PRODUCERS
gateways_th_cloud_l1_arr = np.array(from_csv_to_arr(gateways_th_cloud_l1_csv_files))
producers_th_hybrid_l1_arr = np.array(from_csv_to_arr(producers_th_hybrid_l1_csv_files))  # * NUM_PRODUCERS
gateways_th_hybrid_l1_arr = np.array(from_csv_to_arr(gateways_th_hybrid_l1_csv_files))
check_all_nodes('LARGE', producers_th_cloud_l1_arr, gateways_th_cloud_l1_arr, producers_th_hybrid_l1_arr, gateways_th_hybrid_l1_arr)



# # AVERAGE
# SMALL
producers_th_cloud_s1_mean = np.mean(producers_th_cloud_s1_arr)
gateways_th_cloud_s1_mean = np.mean(gateways_th_cloud_s1_arr)
producers_th_hybrid_s1_mean = np.mean(producers_th_hybrid_s1_arr)
gateways_th_hybrid_s1_mean = np.mean(gateways_th_hybrid_s1_arr)
# MEDIUM
producers_th_cloud_m1_mean = np.mean(producers_th_cloud_m1_arr)
gateways_th_cloud_m1_mean = np.mean(gateways_th_cloud_m1_arr)
producers_th_hybrid_m1_mean = np.mean(producers_th_hybrid_m1_arr)
gateways_th_hybrid_m1_mean = np.mean(gateways_th_hybrid_m1_arr)
# LARGE
producers_th_cloud_l1_mean = np.mean(producers_th_cloud_l1_arr)
gateways_th_cloud_l1_mean = np.mean(gateways_th_cloud_l1_arr)
producers_th_hybrid_l1_mean = np.mean(producers_th_hybrid_l1_arr)
gateways_th_hybrid_l1_mean = np.mean(gateways_th_hybrid_l1_arr)


# # STANDARD DEVIATION
# SMALL
producers_th_cloud_s1_std = np.std(producers_th_cloud_s1_arr)
gateways_th_cloud_s1_std = np.std(gateways_th_cloud_s1_arr)
producers_th_hybrid_s1_std = np.std(producers_th_hybrid_s1_arr)
gateways_th_hybrid_s1_std = np.std(gateways_th_hybrid_s1_arr)
# MEDIUM
producers_th_cloud_m1_std = np.std(producers_th_cloud_m1_arr)
gateways_th_cloud_m1_std = np.std(gateways_th_cloud_m1_arr)
producers_th_hybrid_m1_std = np.std(producers_th_hybrid_m1_arr)
gateways_th_hybrid_m1_std = np.std(gateways_th_hybrid_m1_arr)
# LARGE
producers_th_cloud_l1_std = np.std(producers_th_cloud_l1_arr)
gateways_th_cloud_l1_std = np.std(gateways_th_cloud_l1_arr)
producers_th_hybrid_l1_std = np.std(producers_th_hybrid_l1_arr)
gateways_th_hybrid_l1_std = np.std(gateways_th_hybrid_l1_arr)


# CTEs_c_pd = [producers_th_cloud_s1_mean, producers_th_cloud_m1_mean, producers_th_cloud_l1_mean]
# CTEs_c_gw = [gateways_th_cloud_s1_mean, gateways_th_cloud_m1_mean, gateways_th_cloud_l1_mean]
# CTEs_h_pd = [producers_th_hybrid_s1_mean, producers_th_hybrid_m1_mean, producers_th_hybrid_l1_mean]
# CTEs_h_gw = [gateways_th_hybrid_s1_mean, gateways_th_hybrid_m1_mean, gateways_th_hybrid_l1_mean]

CTEs_c_pd = multiply_arr(producers_th_cloud_s1_mean, producers_th_cloud_m1_mean, producers_th_cloud_l1_mean,
                         len(producers_th_cloud_s1_arr), len(producers_th_cloud_m1_arr), len(producers_th_cloud_l1_arr))
CTEs_c_gw = multiply_arr(gateways_th_cloud_s1_mean, gateways_th_cloud_m1_mean, gateways_th_cloud_l1_mean,
                         len(gateways_th_cloud_s1_arr), len(gateways_th_cloud_m1_arr), len(gateways_th_cloud_l1_arr))
CTEs_h_pd = multiply_arr(producers_th_hybrid_s1_mean, producers_th_hybrid_m1_mean, producers_th_hybrid_l1_mean,
                         len(producers_th_hybrid_s1_arr), len(producers_th_hybrid_m1_arr), len(producers_th_hybrid_l1_arr))
CTEs_h_gw = multiply_arr(gateways_th_hybrid_s1_mean, gateways_th_hybrid_m1_mean, gateways_th_hybrid_l1_mean,
                         len(gateways_th_hybrid_s1_arr), len(gateways_th_hybrid_m1_arr), len(gateways_th_hybrid_l1_arr))


# error_c_pd = [producers_th_cloud_s1_std, producers_th_cloud_m1_std, producers_th_cloud_l1_std]
# error_c_gw = [gateways_th_cloud_s1_std, gateways_th_cloud_m1_std, gateways_th_cloud_l1_std]
# error_h_pd = [producers_th_hybrid_s1_std, producers_th_hybrid_m1_std, producers_th_hybrid_l1_std]
# error_h_gw = [gateways_th_hybrid_s1_std, gateways_th_hybrid_m1_std, gateways_th_hybrid_l1_std]

error_c_pd = multiply_arr(producers_th_cloud_s1_std, producers_th_cloud_m1_std, producers_th_cloud_l1_std,
                         len(producers_th_cloud_s1_arr), len(producers_th_cloud_m1_arr), len(producers_th_cloud_l1_arr))
error_c_gw = multiply_arr(gateways_th_cloud_s1_std, gateways_th_cloud_m1_std, gateways_th_cloud_l1_std,
                         len(gateways_th_cloud_s1_arr), len(gateways_th_cloud_m1_arr), len(gateways_th_cloud_l1_arr))
error_h_pd = multiply_arr(producers_th_hybrid_s1_std, producers_th_hybrid_m1_std, producers_th_hybrid_l1_std,
                         len(producers_th_hybrid_s1_arr), len(producers_th_hybrid_m1_arr), len(producers_th_hybrid_l1_arr))
error_h_gw = multiply_arr(gateways_th_hybrid_s1_std, gateways_th_hybrid_m1_std, gateways_th_hybrid_l1_std,
                         len(gateways_th_hybrid_s1_arr), len(gateways_th_hybrid_m1_arr), len(gateways_th_hybrid_l1_arr))


print(CTEs_c_pd)
print(CTEs_c_gw)
print(CTEs_h_pd)
print(CTEs_h_gw)

print(error_c_pd)
print(error_c_gw)
print(error_h_pd)
print(error_h_gw)


_color_data = 'darkseagreen'
_color_hybrid = 'slategray'  #[0.2, 0.4, 0.6, 0.6]
_color_cloud = 'lightsteelblue'

scenarios = ['Small', 'Medium', 'Large']
ind = np.arange(len(scenarios))*1
width = 0.2
fig, ax = plt.subplots()
# rects1 = ax.bar(ind - 0.3, format_value(CTEs_c_pd), width=width, color=_color_data, yerr=error_c_pd)
# rects2 = ax.bar(ind - 0.1, format_value(CTEs_c_gw), width=width, color=_color_cloud, yerr=error_c_gw)
# rects3 = ax.bar(ind + 0.1, format_value(CTEs_h_pd), width=width, color=_color_data, yerr=error_h_pd)
# rects4 = ax.bar(ind + 0.3, format_value(CTEs_h_gw), width=width, color=_color_hybrid, yerr=error_h_gw)

rects2 = ax.bar(ind - 0.1, format_value(CTEs_c_gw), width=width, color=_color_cloud, yerr=error_c_gw)
rects4 = ax.bar(ind + 0.1, format_value(CTEs_h_gw), width=width, color=_color_hybrid, yerr=error_h_gw)

# draw temporary red and blue lines and use them to create a legend
# line1 = plt.plot([], c=_color_data, label='Generated data')
line2 = plt.plot([], c=_color_cloud, label='Cloud-centric processing')
line3 = plt.plot([], c=_color_hybrid, label='Hybrid processing')

# red_patch = mpatches.Patch(color='red', label='The red data')
# plt.legend()

_fontsize = 12
plt.rcParams.update({'font.size': _fontsize})
plt.xticks(fontsize=_fontsize+1)
plt.yticks(fontsize=_fontsize+1)
ax.set_ylabel('Amount of data (in GB)', fontsize=_fontsize+1)
ax.set_title('Amount of Data Sent to Cloud 10Gb (E-F) / 10Gb (F-C)', fontsize=_fontsize+1)
ax.set_xticks(ind)

ax.set_xticklabels(scenarios)

ax.legend()
ax.yaxis.grid(True)
autolabel(rects2, "left")
autolabel(rects4, "right")
fig.tight_layout()
plt.savefig(f"{plots_path}/tests-new/cost-data-10gbit-10gbit.png")
plt.show()

