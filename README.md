# CCTV

This repository gives you access to the experiments artifacts used in the [IEEE Cluster 2020 paper](https://hal.archives-ouvertes.fr/hal-02916032).
These artifacts refer to applications, libraries, dataset, and configuration files required to run the experiments.
Furthermore, you can access the experiment results and code used to generate the charts.


## Tutorial

The example consists of data producers at the Edge (8 nodes, each one with 40 cameras); gateways at the Fog (4 nodes, each one with a java application to process images); and a Flink Cluster (1 node) and a Kafka Cluster (1 node) at the Cloud.

https://e2clab.gitlabpages.inria.fr/e2clab/examples/cctv.html


